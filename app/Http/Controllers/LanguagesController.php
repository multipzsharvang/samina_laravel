<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Models\Languages;
class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Languages::get();
        return view('backend.languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                $languages = Languages::where('id',$id)->first();
                if (empty($languages->is_active)) {
                    $languages->is_active = 1;
                } else {
                    $languages->is_active = 0;
                }
                    $languages->save();
                return response()->json(['success' => 1]);
            }else{
                $this->validate($request, [
                    'name' => 'required',
                    'code' => 'required',
                ]);
                if(!empty($request->id)){
                    $id = Crypt::decrypt($request->id);
                    $languages = Languages::where('id',$id)->first();
                    $message = trans('message.update', ['entity' => trans('cruds.languages.title_singular')]);
                }else{
                    $languages = new Languages;
                    $message = trans('message.create', ['entity' => trans('cruds.languages.title_singular')]);
                }
                   
                $languages->name = $request->name;
                $languages->code = strtolower(preg_replace("/[^a-zA-Z]+/", "_", $request->code));
                $languages->save();
                return redirect()->route('languages.index')->with('success',$message);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
            return redirect()->back()->with('danger', $e->getMessage());    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Languages  $Languages
     * @return \Illuminate\Http\Response
     */
    public function show(Languages $Languages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Languages  $Languages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $languages = Languages::where('id',$id)->first();
        return view('backend.languages.edit', compact('languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Languages  $Languages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Languages $Languages)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                Languages::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
