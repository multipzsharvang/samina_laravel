<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use DB;

use App\Models\Keyword;
use App\Models\KeywordLanguage;
use App\Models\Languages;

class KeywordController extends Controller
{
    public function index()
    {
        $keyword = Keyword::with('multiple_keyword_type_name')->orderBy('id','desc')->get();
        $language  = Languages::orderBy('id','asc')->get();
        return view('backend.keyword.index', compact('keyword','language'));
    }

    public function create()
    {
        $languages = Languages::all();
        return view('backend.keyword.create', compact('languages'));
    }

    public function store(Request $request)
    {
        $checkKeyword = Keyword::where('keyword',$request->keyword)->first();
        if($checkKeyword){
            return redirect()->back()->with("danger","Label keyword already used");
        }
        $keyword = new Keyword();
        $keyword->keyword = strtolower($request->keyword);
        $keyword->save();

        if ($keyword) {
            $languages = Languages::all();
           foreach ($languages as $language) {
                $lang = $request['language_' . $language->code];
                if ($lang) {
                    $keywordLanguage = new KeywordLanguage();
                    $keywordLanguage->keyword_language = $lang;
                    $keywordLanguage->keyword_id = $keyword->id;
                    $keywordLanguage->language_id = $language->id;
                    $keywordLanguage->save();
                }
            }
        }

        $message = trans('message.create', ['entity' => trans('cruds.keyword.title_singular')]);
        return redirect()->route('keyword.index')->with('success',$message);
    }

    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $language = Languages::get();
        $keyword = Keyword::with('multiple_keyword_type_name')->find($id);
        //$keywordLanguage = KeywordLanguage::where('keyword_id', $keyword->id)->get();

        return view('backend.keyword.edit', compact('keyword', 'language'));
    }

    public function update(Request $request, Keyword $keyword)
    {
        $keywordLanguageDelete = DB::table('keyword_language')->where('keyword_id', $keyword->id)->delete();

        $keyword = Keyword::find($keyword->id);
        $keyword->keyword = strtolower($request->keyword);
        $keyword->save();
        //dd($request->all());
        if ($keyword) {
            $languages = Languages::get();

            foreach ($languages as $language) {
                $lang = $request['language_' . $language->code];
                if ($lang) {
                    $keywordLanguage = new KeywordLanguage();
                    $keywordLanguage->keyword_language = $lang;
                    $keywordLanguage->keyword_id = $keyword->id;
                    $keywordLanguage->language_id = $language->id;
                    $keywordLanguage->save();
                }
            }
        }

        $message = trans('message.update', ['entity' => trans('cruds.keyword.title_singular')]);
        return redirect()->route('keyword.index')->with('success',$message);
    }


    public function destroy(Request $request)
    {
        try{
            if($request->ajax()) {
                $id = Crypt::decrypt($request->id);
                Keyword::where('id',$id)->delete();
                return response()->json(['success' => 1]);
            }
        }catch(\Exception $e){
            if($request->ajax()) {
                 return response()->json(['error' => 1,'message' =>$e->getMessage()]);
            }
        } 
    }
}
