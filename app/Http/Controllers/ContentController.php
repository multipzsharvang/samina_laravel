<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

use App\Models\Content;
use App\Models\Languages;
use App\Helpers\Helper;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::groupBy('code')->get();
        $languages  = Languages::orderBy('id', 'asc')->get();
        return view('backend.content.index', compact('contents', 'languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages  = Languages::orderBy('id', 'asc')->get();
        return view('backend.content.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checkKeyword = Content::where('code', $request->keyword)->first();
        if ($checkKeyword) {
            return redirect()->back()->with("danger", "keyword already used");
        }
        $languages = Languages::all();
        foreach ($languages as $language) {
            $lang = $request['language_' . $language->code];
            if ($lang) {
                $keywordLanguage = new Content;
                $keywordLanguage->keyword_language = $lang;
                $keywordLanguage->code = str_replace(' ','_',strtolower($request->keyword));
                $keywordLanguage->language_id = $language->id;
                $keywordLanguage->save();
            }
        }
        $message = trans('message.create', ['entity' => trans('cruds.content.title_singular')]);
        return redirect()->route('content.index')->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Languages  $Languages
     * @return \Illuminate\Http\Response
     */
    public function show(Languages $Languages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Languages  $Languages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $languages = Languages::orderBy('id', 'asc')->get();
        $keyword = Content::where('id',$id)->first();
        return view('backend.content.edit', compact('languages','keyword'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        $languages = Languages::all();
        foreach ($languages as $language) {
            $lang = $request['language_' . $language->code];
            if ($lang) {
                $keywordLanguage = Content::where(['code' => $content->code,'language_id' => $language->id])->first();
                if(!$keywordLanguage){
                    $keywordLanguage = new Content;
                }
                $keywordLanguage->keyword_language = $lang;
                $keywordLanguage->code = $content->code;
                $keywordLanguage->language_id = $language->id;
                $keywordLanguage->save();
            }
        }
        $message = trans('message.update', ['entity' => trans('cruds.content.title_singular')]);
        return redirect()->route('content.index')->with('success', $message);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // try {
        //     if ($request->ajax()) {
        //         $id = Crypt::decrypt($request->id);
        //         Languages::where('id', $id)->delete();
        //         return response()->json(['success' => 1]);
        //     }
        // } catch (\Exception $e) {
        //     if ($request->ajax()) {
        //         return response()->json(['error' => 1, 'message' => $e->getMessage()]);
        //     }
        // }
    }
}
