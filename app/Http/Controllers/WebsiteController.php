<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Models\Keyword;
use App\Models\KeywordLanguage;
use App\Models\Languages;
use App\Models\Content;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class WebsiteController extends Controller

{

    public function index(Request $request)
    {
        try {
            $data = $this->getOutput();
            $outPutArray = $data['output'];
            $languages = $data['languages'];
            $language_id = $data['language_id'];
            return view('index', compact('outPutArray', 'languages', 'language_id'));
        } catch (\Exception $e) {
            return redirect()->intended(route('index'));
        }
    }
    public function contentPage()
    {
        try {

            $data = $this->getOutput();
            $outPutArray = $data['output'];
            $languages = $data['languages'];
            $language_id = $data['language_id'];

            $routeName = Route::currentRouteName();
            $field_name = '';
            $view_page = '';
            if ($routeName == "privacy.policy") {
                $field_name = 'privacy_policy';
            } else if ($routeName == "terms.of.service") {
                $field_name = 'terms_of_service';
            } else if ($routeName == "cookies.policy") {
                $field_name = 'cookies_policy';
            } else if ($routeName == "returns.policy") {
                $field_name = 'returns_policy';
            } else if ($routeName == "shipping.terms") {
                $field_name = 'shipping_terms';
            }

            $result = Content::where('code', $field_name)->where('language_id', $language_id)->pluck('keyword_language')->first();

            return view('pages.' . $field_name, compact('result', 'outPutArray', 'languages', 'language_id'));
        } catch (\Exception $e) {
            return redirect()->intended(route('index'));
        }
    }
    public function upload(Request $request)
    {
        if ($request->hasFile('upload')) {
            $attachment = $request->file('upload');
            $storage_path = "public/images";
            $imgpath = Storage::disk('local')->put($storage_path, $request->file('upload'));
            //get filename with extension
            //$filenamewithextension = $request->file('upload')->getClientOriginalName();
            //get filename without extension
            //$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            //$extension = $request->file('upload')->getClientOriginalExtension();
            //filename to store
            //$filenametostore = $filename.'_'.time().'.'.$extension;
            //Upload File
            //$request->file('upload')->storeAs('public/uploads', $filenametostore);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            //$url = asset('storage/uploads/'.$filenametostore);
            $url = asset(Storage::url($imgpath));
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
    
    public function userManualEn(){
        return view('manual.en');
    }
    

    public function introduction(){
        return view('manual.en-introduction');
    }
    public function generaldescription(){
        return view('manual.en-generaldescription');
    }
    public function intendeduse(){
        return view('manual.en-intendeduse');
    }
    public function colorlighttherapy(){
        return view('manual.en-colorlighttherapy');
    }
    public function safetyimportant(){
        return view('manual.en-safetyimportant');
    }
    public function electromagnetic(){
        return view('manual.en-electromagnetic');
    }
    public function setupdevice(){
        return view('manual.en-setupdevice');
    }
    public function appledevices(){
        return view('manual.en-appledevices');
    }
    public function readingmode(){
        return view('manual.en-readingmode');
    }
    public function wakeuphelp(){
        return view('manual.en-wakeuphelp');
    }
    public function sleepaid(){
        return view('manual.en-sleepaid');
    }
    public function lightcontrol(){
        return view('manual.en-lightcontrol');
    }
    public function musicmeditations(){
        return view('manual.en-musicmeditations');
    }
    public function lightcolortherapy(){
        return view('manual.en-lightcolortherapy');
    }
    public function personalsleephealthcoach(){
        return view('manual.en-personalsleephealthcoach');
    }
    public function offlineuse(){
        return view('manual.en-offlineuse');
    }
    public function troubleshooting(){
        return view('manual.en-troubleshooting');
    }
    public function storagecare(){
        return view('manual.en-storagecare');
    }
    public function recycling(){
        return view('manual.en-recycling');
    }
    public function warrantyandsupport(){
        return view('manual.en-warrantyandsupport');
    }
    public function technicaldata(){
        return view('manual.en-technicaldata');
    }
    public function updatefirmware(){
        return view('manual.en-updatefirmware');
    }
    
    
    
    public function userManualDe(){
        return view('manual.de');  
    }
    public function deIntroduction(){
        return view('manual.de-introduction');  
    }
    public function deGeneraldescription(){
        return view('manual.de-generaldescription');  
    }
    public function deIntendeduse(){
        return view('manual.de-intendeduse');  
    }
    public function deColorlighttherapy(){
        return view('manual.de-colorlighttherapy');  
    }
    public function deSafetyimportant(){
        return view('manual.de-safetyimportant');  
    }
    public function deElectromagnetic(){
        return view('manual.de-electromagnetic');  
    }
    public function deSetupdevice(){
        return view('manual.de-setupdevice');  
    }
    public function deAppledevices(){
        return view('manual.de-appledevices');  
    }
    public function deReadingmode(){
        return view('manual.de-readingmode');  
    }
    public function deWakeuphelp(){
        return view('manual.de-wakeuphelp');  
    }
    public function deSleepaid(){
        return view('manual.de-sleepaid');  
    }
    public function deLightcontrol(){
        return view('manual.de-lightcontrol');  
    }
    public function deMusicmeditations(){
        return view('manual.de-musicmeditations');  
    }
    public function deLightcolortherapy(){
        return view('manual.de-lightcolortherapy');  
    }
    public function dePersonalsleephealthcoach(){
        return view('manual.de-personalsleephealthcoach');  
    }
    public function deOfflineuse(){
        return view('manual.de-offlineuse');  
    }
    public function deTroubleshooting(){
        return view('manual.de-troubleshooting');  
    }
    public function deStoragecare(){
        return view('manual.de-storagecare');  
    }
    public function deRecycling(){
        return view('manual.de-recycling');  
    }
    public function deWarrantyandsupport(){
        return view('manual.de-warrantyandsupport');  
    }
    public function deTechnicaldata(){
        return view('manual.de-technicaldata');  
    }
    public function deUpdatefirmware(){
        return view('manual.de-updatefirmware');  
    }
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function getOutput()
    {
        $code = "en";
        $language_id = 1;
        $ip = "";
        if (isset($_COOKIE["select_language_id"])) {
            // Store the IP address
            $language_id = $_COOKIE["select_language_id"];
        } else {
            $ip = $this->getVisIPAddr();
            //$ip = "46.19.138.178"; // german ip
            //$ip = "202.131.126.166"; // india ip
        }


        if (!empty($ip)) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://www.geoplugin.net/json.gp?ip=" . $ip,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "postman-token: 6d5f36a0-3108-1d8e-737c-569ac79ec73d"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if (!$err) {
                $response = json_decode($response, true);
                if (!empty($response)) {
                    $country_code = $response['geoplugin_countryCode'];

                    // "geoplugin_request" => "198.16.66.100"
                    // "geoplugin_status" => 200
                    // "geoplugin_delay" => "2ms"
                    // "geoplugin_credit" => "Some of the returned data includes GeoLite data created by MaxMind, available from <a href='http://www.maxmind.com'>http://www.maxmind.com</a>."
                    // "geoplugin_city" => "Amsterdam"
                    // "geoplugin_region" => "North Holland"
                    // "geoplugin_regionCode" => "NH"
                    // "geoplugin_regionName" => "North Holland"
                    // "geoplugin_areaCode" => ""
                    // "geoplugin_dmaCode" => ""
                    // "geoplugin_countryCode" => "NL"
                    // "geoplugin_countryName" => "Netherlands"
                    // "geoplugin_inEU" => 1
                    // "geoplugin_euVATrate" => 21
                    // "geoplugin_continentCode" => "EU"
                    // "geoplugin_continentName" => "Europe"
                    // "geoplugin_latitude" => "52.3716"
                    // "geoplugin_longitude" => "4.8883"
                    // "geoplugin_locationAccuracyRadius" => "1000"
                    // "geoplugin_timezone" => "Europe/Amsterdam"
                    // "geoplugin_currencyCode" => "EUR"
                    // "geoplugin_currencySymbol" => "€"
                    // "geoplugin_currencySymbol_UTF8" => "€"
                    // "geoplugin_currencyConverter" => 0.8813

                    //dd($country_code);
                    if (!empty($country_code)) {
                        //$country_code = "en";
                        $country_code = str_replace(' ', '_', strtolower($country_code));
                        $euro_country_array = array('de', 'deu', 'de_li', 'de-li', 'de-de', 'de_de', 'pa', 'de-ch', 'de_ch', 'li', 'at', 'ch', 'nl', 'pa');
                        if ($country_code == "in") {
                            $country_code = "en";
                        } else if (in_array($country_code, $euro_country_array)) {
                            $country_code = "de";
                        }
                        $checkCode = Languages::where('code', $country_code)->first();
                        if ($checkCode) {
                            $language_id = $checkCode->id;

                            // setcookie("select_language_id", $language_id);
                        }
                    }
                }
            }
        }

        $multilanguage = Keyword::whereHas('keyword_type_name', function ($query) use ($language_id) {
            $query->where('language_id', $language_id);
        })->with(['keyword_type_name' => function ($query) use ($language_id) {
            $query->where('language_id', $language_id);
        }])->get();

        $outPutArray = [];
        foreach ($multilanguage as $value) {
            $outPutArray[$value->keyword] = "";
            if ($value->keyword_type_name) {
                $outPutArray[$value->keyword] = $value->keyword_type_name->keyword_language;
            }
        }
        $tempArray = [];
        $languages  = Languages::where('is_active', 1)->get();
        $tempArray['output'] =  $outPutArray;
        $tempArray['languages'] =  $languages;
        $tempArray['language_id'] =  $language_id;
        return $tempArray;
    }
    function getVisIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }
}
