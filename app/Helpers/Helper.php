<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use App\Models\Content;
class Helper
{
    public static function getContent($keyword,$language_id)
    {
        $output = "";
        $check = Content::where(['code'=> $keyword,'language_id' => $language_id])->first();
        if($check){
            $output = $check->keyword_language;
        }
        return $output;
    }
}