<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Die :attribute muss akzeptiert werden.',
    'active_url' => 'Die :attribute ist keine g�ltige URL.',
    'after' => 'Die :attribute muss ein Datum sein, das nach :date.',
    'after_or_equal' => 'Die :attribute muss ein Datum sein, das nach oder gleich :date.',
    'alpha' => 'Die :attribute darf nur Buchstaben enthalten.',
    'alpha_dash' => 'Die :attribute darf nur Buchstaben, Zahlen, Bindestriche und Unterstriche enthalten.',
    'alpha_num' => 'Die :attribute darf nur Buchstaben und Zahlen enthalten.',
    'array' => 'Die :attribute muss ein Array sein.',
    'before' => 'Die :attribute muss ein Datum sein, das vor :date.',
    'before_or_equal' => 'Die :attribute muss ein Datum sein, das vor oder gleich dem :date.',
    'between' => [
        'numeric' => 'Die :attribute muss zwischen :min und :max.',
        'file' => 'Die :attribute muss zwischen :min und :max kilobytes.',
        'string' => 'Die :attribute muss zwischen :min und :max Zeichen.',
        'array' => 'Die :attribute m�ssen zwischen :min und :max artikel.',
    ],
    'boolean' => 'Die :attribute feld muss true oder false sein.',
    'confirmed' => 'Die :attribute Best�tigung stimmt nicht �berein.',
    'date' => 'Die :attribute ist kein g�ltiges Datum.',
    'date_equals' => 'Die :attribute muss ein Datum sein, das gleich :date.',
    'date_format' => 'Die :attribute entspricht nicht dem Format :format.',
    'different' => 'Die :attribute und :other m�ssen unterschiedlich sein.',
    'digits' => 'Die :attribute muss sein :digits ziffern.',
    'digits_between' => 'Die :attribute muss zwischen :min und :max ziffern.',
    'dimensions' => 'Die :attribute hat ung�ltige Bildabmessungen.',
    'distinct' => 'Die :attribute feld hat einen doppelten Wert.',
    'email' => 'Die :attribute muss eine g�ltige E-Mail Adresse sein.',
    'exists' => 'Die ausgew�hlt :attribute ist ung�ltig.',
    'file' => 'Die :attribute muss eine Datei sein.',
    'filled' => 'Die :attribute feld muss einen Wert haben.',
    'gt' => [
        'numeric' => 'Die :attribute muss gr��er sein als :value.',
        'file' => 'Die :attribute muss gr��er sein als :value kilobytes.',
        'string' => 'Die :attribute muss gr��er sein als :value zeichen.',
        'array' => 'Die :attribute muss mehr haben als :value artikel.',
    ],
    'gte' => [
        'numeric' => 'Die :attribute muss gr��er oder gleich sein :value.',
        'file' => 'Die :attribute muss gr��er oder gleich sein :value kilobytes.',
        'string' => 'Die :attribute muss gr��er oder gleich sein :value zeichen.',
        'array' => 'Die :attribute muss haben :value artikel oder mehr.',
    ],
    'image' => 'Die :attribute muss ein Bild sein.',
    'in' => 'Die ausgew�hlt :attribute ist ung�ltig.',
    'in_array' => 'Die :attribute feld existiert nicht in :other.',
    'integer' => 'Die :attribute muss eine ganze Zahl sein.',
    'ip' => 'Die :attribute muss eine g�ltige IP-Adresse sein.',
    'ipv4' => 'Die :attribute muss eine g�ltige IPv4-Adresse sein.',
    'ipv6' => 'Die :attribute muss eine g�ltige IPv6-Adresse sein.',
    'json' => 'Die :attribute muss eine g�ltige JSON-Zeichenkette sein.',
    'lt' => [
        'numeric' => 'Die :attribute muss kleiner sein als :value.',
        'file' => 'Die :attribute muss kleiner sein als :value kilobytes.',
        'string' => 'Die :attribute muss kleiner sein als :value zeichen.',
        'array' => 'Die :attribute muss weniger haben als :value artikel.',
    ],
    'lte' => [
        'numeric' => 'Die :attribute muss kleiner sein als or equal :value.',
        'file' => 'Die :attribute muss kleiner sein als or equal :value kilobytes.',
        'string' => 'Die :attribute muss kleiner sein als or equal :value zeichen.',
        'array' => 'Die :attribute darf nicht mehr haben als :value artikel.',
    ],
    'max' => [
        'numeric' => 'Die :attributedarf nicht gr��er sein als :max.',
        'file' => 'Die :attribute darf nicht gr��er sein als :max kilobytes.',
        'string' => 'Die :attribute darf nicht gr��er sein als :max zeichen.',
        'array' => 'Die :attribute darf nicht mehr haben als :max artikel.',
    ],
    'mimes' => 'Die :attribute muss eine Datei der type: :values.',
    'mimetypes' => 'Die :attribute muss eine Datei der type: :values.',
    'min' => [
        'numeric' => 'Die :attribute muss mindestens sein :min.',
        'file' => 'Die :attribute muss mindestens sein :min kilobytes.',
        'string' => 'Die :attribute muss mindestens sein :min zeichen.',
        'array' => 'Die :attribute muss mindestens :min artikel.',
    ],
    'not_in' => 'Die ausgew�hlt :attribute ist ung�ltig.',
    'not_regex' => 'Die :attribute format ist ung�ltig.',
    'numeric' => 'Die :attribute muss eine Zahl sein.',
    'present' => 'Die :attribute feld muss vorhanden sein.',
    'regex' => 'Die :attribute format ist ung�ltig.',
    'required' => 'Die :attribute feld ist erforderlich.',
    'required_if' => 'Die :attribute feld ist erforderlich, wenn :other ist :value.',
    'required_unless' => 'Die :attribute ist ein Pflichtfeld, es sei denn :other ist in :values.',
    'required_with' => 'Die :attribute feld ist erforderlich, wenn :values vorhanden ist.',
    'required_with_all' => 'Die :attribute feld ist erforderlich, wenn :values vorhanden sind.',
    'required_without' => 'Die :attribute feld ist erforderlich, wenn :values nicht vorhanden ist.',
    'required_without_all' => 'Die :attribute feld ist erforderlich, wennfield ist erforderlich, wenn keiner der :values vorhanden sind.',
    'same' => 'Die :attribute and :other must match.',
    'size' => [
        'numeric' => 'Die :attribute muss sein :size.',
        'file' => 'Die :attribute muss sein :size kilobytes.',
        'string' => 'Die :attribute muss sein :size zeichen.',
        'array' => 'Die :attribute muss enthalten :size artikel.',
    ],
    'starts_with' => 'Die :attribute muss mit einem der folgenden Begriffe beginnen following: :values',
    'string' => 'Die :attribute muss eine Zeichenkette sein.',
    'timezone' => 'Die :attribute muss ein g�ltiges Gebiet sein.',
    'unique' => 'Die :attribute wurde bereits eingenommen.',
    'uploaded' => 'Die :attribute Upload fehlgeschlagen.',
    'url' => 'Die :attribute format ist ung�ltig.',
    'uuid' => 'Die :attribute muss eine g�ltige UUID sein.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
