<?php
return [
    'create' => ':entity erfolgreich erstellt',
    'update' => ':entity erfolgreich aktualisiert',
    'delete' => ':entity erfolgreich gel�scht',
    'new'    => ':entity neu hinzugekommen'
];    