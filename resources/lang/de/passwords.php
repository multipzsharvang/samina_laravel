<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Die Passw�rter m�ssen mindestens acht Zeichen lang sein und mit der Best�tigung �bereinstimmen.',
    'reset' => 'Ihr Passwort wurde zur�ckgesetzt!',
    'sent' => 'Wir haben Ihnen den Link zum Zur�cksetzen Ihres Passworts per E-Mail geschickt!',
    'token' => 'Dieses Passwort-Reset-Token ist ung�ltig.',
    'user' => "Wir k�nnen keinen Benutzer mit dieser E-Mail-Adresse finden.",

];
