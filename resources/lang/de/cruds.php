<?php

return [
    'menu' => 'Menü',
    'setting' => 'Einstellungen',
    'custom_required' => '*Das Feld ist erforderlich.',
    'permission'     => [
        'title'          => 'Berechtigungen',
        'title_singular' => 'Berechtigungen',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Titel',
            'title_helper'      => '',
            'created_at'        => 'Erstellt am',
            'created_at_helper' => '',
            'updated_at'        => 'Aktualisiert am',
            'updated_at_helper' => '',
            'deleted_at'        => 'Gelöscht am',
            'deleted_at_helper' => '',
        ],
    ],
    'settings'     => [
        'title'          => 'Einstellungen',
        'title_singular' => 'Einstellungen',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'dashboard'     => [
        'title'          => 'Dashboard',
        'title_singular' => 'Dashboard',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'languages'     => [
        'title'          => 'Sprachen',
        'title_singular' => 'Sprachen',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'code'             => 'Code',
        ],
    ],
    'keyword'     => [
        'title'          => 'Label-Verwaltung',
        'title_singular' => 'Label-Verwaltung',
        'fields'         => [
            'id'              => 'ID',
            'name'            => 'Name',
            'keyword'           => 'Schlüsselwort',
        ],
    ],
    'faq'     => [
        'title'          => 'FAQ',
        'title_singular' => 'FAQ',
        'fields'         => [
            'id'              => 'ID',
            'question'        => 'Frage',
            'answer'          => 'Antwort',
        ],
    ],
    'ticketcategory'     => [
        'title'          => 'Ticket-Kategorie',
        'title_singular' => 'Ticket-Kategorie',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name'
        ],
    ],
    'musictype'     => [
        'title'          => 'Musik Typ',
        'title_singular' => 'Musik Typ',
        'fields'         => [
            'id'                => 'ID',
            'type'             => 'Typ'
        ],
    ],
    'artist'     => [
        'title'          => 'Künstler',
        'title_singular' => 'Künstler',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Bild'
        ],
    ],
    'category'     => [
        'title'          => 'Kategorie',
        'title_singular' => 'Kategorie',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Bild'
        ],
    ],
    'alarmdurationtime'     => [
        'title'          => 'Alarmdauer',
        'title_singular' => 'Alarmdauer',
        'fields'         => [
            'id'                => 'ID',
            'time'             => 'Zeit'
        ],
    ],
    'alarmtype'     => [
        'title'          => 'Alarm Typ',
        'title_singular' => 'Alarm Typ',
        'fields'         => [
            'id'          => 'ID',
            'name'        => 'Name',
            'type'        => 'Typ',
            'image'       => 'Bild'
        ],
    ],
    'supportticket'     => [
        'title'          => 'Support-Ticket',
        'title_singular' => 'Support-Ticket',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'category_type' => 'Kategorie Typ',
            'title' => 'Titel',
            'description' => 'Beschreibung',
            'status' => 'Status',
            'comment' => 'Kommentar',
        ],
    ],
    'transactionhistory'     => [
        'title'          => 'Transaktionsverlauf',
        'title_singular' => 'Transaktionsverlauf',
        'fields'         => [
            'id'              => 'ID',
            'category'        => 'Kategorie',
            'name'            => 'Name',
            'music'           => 'Musik',
            'transaction_id'  => 'Transaktions-ID',
            'price'           => 'Preis',
            'date'            => 'Datum',
        ],
    ],
    'content'     => [
        'title'          => 'Inhalt',
        'title_singular' => 'Inhalt',
        'fields'         => [
            'id'           => 'ID',
            'title'        => 'Titel',
            'description'  => 'Beschreibung',
            'slug'         => 'Schnecke'
        ],
    ],
    'mobileversion'     => [
        'title'          => 'Mobile Version',
        'title_singular' => 'Mobile Version',
        'fields'         => [
            'id'                => 'ID',
            'current_version'   => 'Aktuelle Version',
            'forcefully_update' => 'Erzwungenes Update',
            'device_type'       => 'Gerätetyp'
        ],
    ],
    'music'     => [
        'title'          => 'Musik',
        'title_singular' => 'Musik',
        'fields'         => [
            'id'            => 'ID',
            'image'         => 'Bild',
            'name'          => 'Name',
            'artist_id'     => 'Künstler-ID',
            'artist'        => 'Künstler',
            'category_id'   => 'Kategorie Id',
            'category'      => 'Kategorie',
            'price'         => 'Preis',
            'type_id'       => 'Typenbezeichnung',
            'type'          => 'Typ',
            'path'          => 'Pfad',
            'wake_up'       => 'Aufwachen',
            'duration'      => 'Dauer'
        ],
    ],
    'dashboard'     => [
        'title'          => 'Dashboard',
        'title_singular' => 'Dashboard',
        'fields'         => [
            'id'               => 'ID',
            'orders'           => 'Bestellungen',
            'last_orders'           => 'Letzte 10 Bestellungen',
            'users'            => 'Benutzer',
            'total_price'      => 'Gesamtpreis',
            'music'            => 'Musik',
            'user'             => 'Benutzer',
            'price'            => 'Preis',
            'selling_music'    => 'Top 5 der meistverkauften Musik',
            'sales_report'     => 'Verkaufsbericht',
            'artist'           => 'Künstler'
        ],
    ],
    'user'     => [
        'title'          => 'Benutzerverwaltung',
        'title_singular' => 'Benutzerverwaltung',
        'fields'         => [
            'id'             => 'ID',
            'name'           => 'Name',
            'username'       => 'Nutzername',
            'gender'         => 'Geschlecht',
            'weight'         => 'Gewicht',
            'height'         => 'Höhe',
            'device_type'    => 'Gerätetyp',
            'age'            => 'Alter',
            'height_unit'    => 'Höhe Einheit',
            'weight_unit'     => 'Gewicht Einheit',
            'artist'          => 'Künstler',
            'user_address'    => 'Benutzeradresse',
            'address'         => 'Adresse',
            'country'         => 'Land',
            'state'           => 'Staat',
            'city'            => 'Stadt',
            'zipcode'         => 'Postleitzahl',
            'device_id'       => 'Geräte-ID',
            'user_transaction_history'=>'Benutzer-Transaktionsverlauf'
        ],
    ],
    'ticketscomments'     => [
        'title'          => 'Tickets Kommentare',
        'title_singular' => 'Tickets Kommentare',
        'fields'         => [
            'id'                => 'ID',
            'description'   => 'Beschreibung'
        ],
    ],
    'therapy'     => [
        'title'          => 'Therapie',
        'title_singular' => 'Therapie',
        'fields'         => [
            'id'      => 'ID',
            'title'   => 'Titel',
            'description'   => 'Beschreibung',
            'music'   => 'Musik',
            'image'   => 'Bild',
            'time'   => 'Zeit',
            'color'  => 'Farbe',
            'color_name'  => 'Farbe Name',
            'in_minute' => 'in Minutenschnelle',
            'therapyType' => 'Therapie-Typ',
            'price' => 'Preis',
            'type' => 'Typ'
        ],
    ],
    'therapy_type'     => [
        'title'          => 'Therapie-Typ',
        'title_singular' => 'Therapie-Typ',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Bild'
        ],
    ],
    'frequency_type'     => [
        'title'          => 'Frequenz Typ',
        'title_singular' => 'Frequenz Typ',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Bild',
            'color' => 'Farbe',
            'type' => 'Typ'
        ],
    ],
    'frequencytype'     => [
        'title'          => 'Frequenz Typ',
        'title_singular' => 'Frequenz Typ',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Bild',
            'color' => 'Farbe',
            'type' => 'Typ'
        ],
    ],
    'qrcode'     => [
        'title'          => 'Qr-Code',
        'title_singular' => 'Qr-Code',
        'fields'         => [
            'id'    => 'ID',
            'code'  => 'Code',
            'number' => 'Nummer'
        ],
    ],
];
