<?php
return [
    'create' => ':entity created successfully',
    'update' => ':entity updated successfully',
    'delete' => ':entity deleted successfully',
    'new'    => ':entity new added'
];    