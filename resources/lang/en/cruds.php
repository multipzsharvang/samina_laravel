<?php

return [
    'menu' => 'Menu',
    'setting' => 'Settings',
    'custom_required' => '*The field is required.',
    'permission'     => [
        'title'          => 'Permissions',
        'title_singular' => 'Permission',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Title',
            'title_helper'      => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'settings'     => [
        'title'          => 'Settings',
        'title_singular' => 'Settings',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'dashboard'     => [
        'title'          => 'Dashboard',
        'title_singular' => 'Dashboard',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
        ],
    ],
    'languages'     => [
        'title'          => 'Languages',
        'title_singular' => 'Languages',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'code'             => 'Code',
        ],
    ],
    'keyword'     => [
        'title'          => 'Label Management',
        'title_singular' => 'Label Management',
        'fields'         => [
            'id'              => 'ID',
            'name'            => 'Name',
            'keyword'           => 'Keyword',
        ],
    ],
    'faq'     => [
        'title'          => 'FAQ',
        'title_singular' => 'FAQ',
        'fields'         => [
            'id'              => 'ID',
            'question'        => 'Question',
            'answer'          => 'Answer',
        ],
    ],
    'ticketcategory'     => [
        'title'          => 'Ticket Category',
        'title_singular' => 'Ticket Category',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name'
        ],
    ],
    'musictype'     => [
        'title'          => 'Music Type',
        'title_singular' => 'Music Type',
        'fields'         => [
            'id'                => 'ID',
            'type'             => 'Type'
        ],
    ],
    'artist'     => [
        'title'          => 'Artist',
        'title_singular' => 'Artist',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Image'
        ],
    ],
    'category'     => [
        'title'          => 'Category',
        'title_singular' => 'Category',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Image'
        ],
    ],
    'alarmdurationtime'     => [
        'title'          => 'Alarm Duration',
        'title_singular' => 'Alarm Duration',
        'fields'         => [
            'id'                => 'ID',
            'time'             => 'Time'
        ],
    ],
    'alarmtype'     => [
        'title'          => 'Alarm Type',
        'title_singular' => 'Alarm Type',
        'fields'         => [
            'id'          => 'ID',
            'name'        => 'Name',
            'type'        => 'Type',
            'image'       => 'Image'
        ],
    ],
    'supportticket'     => [
        'title'          => 'Support Ticket',
        'title_singular' => 'Support Ticket',
        'fields'         => [
            'id'                => 'ID',
            'name'             => 'Name',
            'category_type' => 'Category Type',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'comment' => 'Comment',
        ],
    ],
    'transactionhistory'     => [
        'title'          => 'Transaction History',
        'title_singular' => 'Transaction History',
        'fields'         => [
            'id'              => 'ID',
            'category'        => 'Category',
            'name'            => 'Name',
            'music'           => 'Music',
            'transaction_id'  => 'Transaction Id',
            'price'           => 'Price',
            'date'            => 'Date',
            'action'          => 'Action'
        ],
    ],
    'content'     => [
        'title'          => 'Content',
        'title_singular' => 'Content',
        'fields'         => [
            'id'           => 'ID',
            'title'        => 'Title',
            'description'  => 'Description',
            'slug'         => 'Slug'
        ],
    ],
    'mobileversion'     => [
        'title'          => 'Mobile Version',
        'title_singular' => 'Mobile Version',
        'fields'         => [
            'id'                => 'ID',
            'current_version'   => 'Current Version',
            'forcefully_update' => 'Forcefully Update',
            'device_type'       => 'Device Type'
        ],
    ],
    'music'     => [
        'title'          => 'Music',
        'title_singular' => 'Music',
        'fields'         => [
            'id'            => 'ID',
            'image'         => 'Image',
            'name'          => 'Name',
            'artist_id'     => 'Artist Id',
            'artist'        => 'Artist',
            'category_id'   => 'Category Id',
            'category'      => 'Category',
            'price'         => 'Price',
            'type_id'       => 'Type Id',
            'type'          => 'Type',
            'path'          => 'Path',
            'wake_up'       => 'Wake Up',
            'duration'      => 'Duration'
        ],
    ],
    'dashboard'     => [
        'title'          => 'Dashboard',
        'title_singular' => 'Dashboard',
        'fields'         => [
            'id'               => 'ID',
            'orders'           => 'Orders',
            'last_orders'           => 'Last 10 Orders',
            'users'            => 'Users',
            'total_price'      => 'Total Price',
            'music'            => 'Music',
            'user'             => 'User',
            'price'            => 'Price',
            'selling_music'    => 'Top 5 Selling Music',
            'sales_report'     => 'Sales Report',
            'artist'           => 'Artist'
        ],
    ],
    'user'     => [
        'title'          => 'User Management',
        'title_singular' => 'User Management',
        'fields'         => [
            'id'             => 'ID',
            'name'           => 'Name',
            'username'       => 'Usersname',
            'gender'         => 'Gender',
            'weight'         => 'Weight',
            'height'         => 'Height',
            'device_type'    => 'Device Type',
            'age'            => 'Age',
            'height_unit'    => 'Height Unit',
            'weight_unit'     => 'Weight Unit',
            'artist'          => 'Artist',
            'user_address'    => 'User Address',
            'address'         => 'Address',
            'country'         => 'Country',
            'state'           => 'State',
            'city'            => 'City',
            'zipcode'         => 'Zipcode',
            'device_id'       => 'Device Id',
            'user_transaction_history' => 'User Transaction History'
        ],
    ],
    'ticketscomments'     => [
        'title'          => 'Tickets Comments',
        'title_singular' => 'Tickets Comments',
        'fields'         => [
            'id'                => 'ID',
            'description'   => 'Description'
        ],
    ],
    'therapy'     => [
        'title'          => 'Therapy',
        'title_singular' => 'Therapy',
        'fields'         => [
            'id'      => 'ID',
            'title'   => 'Title',
            'description'   => 'Description',
            'music'   => 'Music',
            'image'   => 'Image',
            'time'   => 'Time',
            'color'  => 'Color',
            'color_name'  => 'Color Name',
            'in_minute' => 'in minute',
            'therapyType' => 'Therapy Type',
            'price' => 'Price',
            'type' => 'Type'
        ],
    ],
    'therapy_type'     => [
        'title'          => 'Therapy Type',
        'title_singular' => 'Therapy Type',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Image'
        ],
    ],
    'frequency_type'     => [
        'title'          => 'Frequency Type',
        'title_singular' => 'Frequency Type',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Image',
            'color' => 'Color',
            'type' => 'Type'
        ],
    ],
    'frequencytype'     => [
        'title'          => 'Frequency Type',
        'title_singular' => 'Frequency Type',
        'fields'         => [
            'id'    => 'ID',
            'name'  => 'Name',
            'image' => 'Image',
            'color' => 'Color',
            'type' => 'Type'
        ],
    ],
    'qrcode'     => [
        'title'          => 'Qr Code',
        'title_singular' => 'Qr Code',
        'fields'         => [
            'id'    => 'ID',
            'code'  => 'Code',
            'number' => 'Number',
            'image' => 'Image',
        ],
    ],
    'content'     => [
        'title'          => 'Contents',
        'title_singular' => 'Content',
        'fields'         => [
            'id'    => 'ID',
            'keyword'           => 'Keyword',
        ],
    ],
];
