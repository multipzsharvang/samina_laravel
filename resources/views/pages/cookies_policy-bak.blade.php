@extends('layouts.front_end')
@section('content')

<div class="py-5">
    <div class="container pb-5 mb-5">
        <!-- {!! $result !!} -->
        <h1 class="h2 text-primary">Cookie Notice</h1>
        <h3 class="h3 mt-3">More Information about Cookie</h3>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum pretium laoreet. Sed eget eros at leo tincidunt consectetur vel dictum ex. Morbi tempus felis dolor, eu tempor massa sodales ac. Mauris laoreet sagittis placerat. Aenean et libero suscipit, condimentum nisl vitae, consequat tellus. Etiam sit amet consectetur orci. Curabitur vehicula, elit eu suscipit mollis, nisl dui pharetra ante, non tincidunt velit justo vel elit. Vestibulum ultrices ornare viverra.
        </p>

        <h3 class="h3 mt-3">More Information about Cookie</h3>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum pretium laoreet. Sed eget eros at leo tincidunt consectetur vel dictum ex. Morbi tempus felis dolor, eu tempor massa sodales ac. Mauris laoreet sagittis placerat. Aenean et libero suscipit, condimentum nisl vitae, consequat tellus. Etiam sit amet consectetur orci. Curabitur vehicula, elit eu suscipit mollis, nisl dui pharetra ante, non tincidunt velit justo vel elit. Vestibulum ultrices ornare viverra.
        </p>
    </div>
</div>
@endsection
@section('script')
@endsection