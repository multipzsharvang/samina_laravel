@extends('layouts.front_end')
@section('content')

<div class="py-5">
    <div class="container pb-5 mb-5">
        <!-- {!! $result !!} -->
        <h1 class="h2 text-primary"> {{ ($outPutArray && array_key_exists("privacy_policy_title",$outPutArray)) ? $outPutArray['privacy_policy_title'] :  '' }} </h1>
        <p>
            {{ ($outPutArray && array_key_exists("privacy_title_desc",$outPutArray)) ? $outPutArray['privacy_title_desc'] :  '' }}    
        </p>
        <div class="accordianmain mt-5">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item mb-4">
                    <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button  d-flex py-4 px-3 px-sm-4 px-lg-5" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <img src="{{ asset('public/assets/frontend/img/icons/newspaper.png') }}" alt="" class="me-3" />
                        <div class="acctitledtl">
                            <h4 class="h4 mb-0 text-primary">Introduction</h4>
                            <span class="text-dark">Read more</span>
                        </div>
                    </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                    </div>
                    </div>
                </div>

                <div class="accordion-item mb-4">
                    <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button d-flex py-4 px-3 px-sm-4 px-lg-5" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <img src="{{ asset('public/assets/frontend/img/icons/newspaper.png') }}" alt="" class="me-3" />
                        <div class="acctitledtl">
                            <h4 class="h4 mb-0 text-primary">What personal data do we collect and for which purpose?</h4>
                            <span class="text-dark">Read more</span>
                        </div>
                    </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu sem sagittis, dictum tortor sit amet, congue justo. Vestibulum tristique, augue vulputate faucibus vestibulum, leo metus consequat enim, vitae bibendum felis enim sed urna. Praesent convallis dolor ut scelerisque varius. Praesent et augue nec quam sollicitudin gravida. Etiam et mauris a tellus mattis pulvinar. Nulla facilisi. Vestibulum malesuada eu turpis a dictum. Proin elementum massa ut odio elementum tincidunt. Fusce molestie, turpis quis dignissim congue, tortor diam luctus arcu, in convallis nunc elit non augue.</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection