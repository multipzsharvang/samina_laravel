<!DOCTYPE html>
<html lang="de" translate="no">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="google" content="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-55BGMB4');</script>
	<!-- End Google Tag Manager -->
	<link rel="shortcut icon" href="{{ asset('public/assets/frontend/img/favicon.ico') }}" type="image/x-icon">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;700&display=swap" rel="stylesheet">
	<title>Samina | Sleepsystem</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
	<link rel="stylesheet" href="{{ asset('public/assets/frontend/css/circle.css') }}">
	<link rel="stylesheet" href="{{ asset('public/assets/frontend/css/custom.css') }}">
</head>

<body>
		
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src=https://www.googletagmanager.com/ns.html?id=GTM-55BGMB4
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div id="loader">
		<span class="loading"></span>
	</div>
	<header class="header">
		<div class="container">
			<div class="header-container">
				<span class="burger-menu">
					<span></span>
					<span></span>
					<span></span>
				</span>
				<a href="{{route('frontpage')}}">
					<img src="{{ asset('public/assets/frontend/img/logo.png') }}" alt="Samina" class="logo">
				</a>
				<a href="https://www.samina.com/samina/sound-light-sleepsystem/" target="_blank" class="btn btn-outline-primary btn-buy pulse">
					<i class="fas fa-shopping-bag"></i> <span class="d-none d-lg-inline-block align-middle">{!! ($outPutArray && array_key_exists("order_now",$outPutArray)) ? $outPutArray['order_now'] :  '' !!}</span>
				</a>
			</div>
		</div>
	</header>
	@yield('content')
	<footer id="footer" class="footer pt-lg-5 pt-3 pb-3 text-center text-lg-start position-relative">
		<div class="container text-light">
			<div class="row">
				<div class="col-md-12 col-lg-4 mb-4 mb-lg-0 order-md-1">
					<h3 class="h3 mb-2">App Download</h3>
					<p>{!! ($outPutArray && array_key_exists("footer_app_download",$outPutArray)) ? $outPutArray['footer_app_download'] :  '' !!}</p>
					<nav class="row justify-content-center justify-content-lg-start">
						<a href="https://play.google.com/store/apps/details?id=com.applie.samina" title="Play Store" class="d-inline-block col-6 col-md-auto mb-2" target="_blank" rel="noopener noreferrer">
							<img src="{{ asset('public/assets/frontend/img/play-store.png')}}" alt="" class="img-fluid">
						</a>
						<a href="https://apps.apple.com/us/app/samina/id1604234417" title="App Store" class="d-inline-block col-6 col-md-auto mb-2" target="_blank" rel="noopener noreferrer">
							<img src="{{ asset('public/assets/frontend/img/app-store.png')}}" alt="" class="img-fluid">
						</a>
					</nav>
				</div>
				<div class="col-md-12 col-lg-4 mb-4 mb-lg-0 order-md-0">
					<h3 class="h3">Contact us</h3>
					<a class="h3 text-light" href="mailto:support@samina.io">support@samina.io</a>
					<h3 class="h3 mt-3 mt-md-5">Follow us</h3>
					<nav class="social-icon mb-3">
						<a href="https://instagram.com/samina.official" target="_blank" rel="noopener noreferrer" title="Instagram"><i class="fab fa-instagram"></i></a>
						<a href="https://www.facebook.com/samina.schlaf/" target="_blank" rel="noopener noreferrer" title="Facebook"><i class="fab fa-facebook-f"></i></a>
						<!-- <a href="#" title="Twitter"><i class="fab fa-twitter"></i></a> -->
					</nav>
					<select class="selectpicker selectlanaguePicker" data-width="fit" name="language_id">
						@foreach($languages as $language)
						<?php $code = $language->code;
						if ($language->code == "en") $code = "gb"; ?>
						<option value="{{$language->id}}" <?php if ($language->id == $language_id) {
																echo " selected ";
															} ?>data-content='<span class="flag-icon flag-icon-{{$code}}"></span> {{ucfirst($language->name)}}'>{{ucfirst($language->name)}}</option>
						@endforeach
						<!-- <option data-content='<span class="flag-icon flag-icon-de"></span> German'>German</option>
							<option data-content='<span class="flag-icon flag-icon-in"></span> Hindi'>Hindi</option> -->
					</select>
				</div>
				<div class="col-md-12 col-lg-4 text-center mb-2 mb-lg-0 order-md-2">
					<h6 class="h4 mb-0 fw-bold">SAMINA Sound Light Sleepsystem</h6>
					<small style="font-size: 12px;">(Wooden Lamp, Hardware & Software, Apps & Website)</small>
					<p class="mb-0 mt-3">Designed, Developed & Manufactured<br />by</p> <a href="http://applie.li" target="_blank" rel="noopener noreferrer"><img src="{{ asset('public/assets/frontend/img/applie-new-logo.png') }}" alt="" class="img-fluid" style="max-height: 48px;"></a>
				</div>

				<div class="col-12 mt-lg-3 text-center order-md-3">
					<ul class="list-inline policy-menu mt-3 mt-lg-4">
						<li class="list-inline-item @if(request()->is('privacy-policy')) active @endif "><a href="{{ route('privacy.policy') }}">Privacy Policy</a> <span class="ms-2">|</span></li>
						<li class="list-inline-item @if(request()->is('terms-of-service')) active @endif "><a href="{{ route('terms.of.service') }}">Terms of Service</a><span class="ms-2">|</span></li>
						<li class="list-inline-item @if(request()->is('cookies-policy')) active @endif "><a href="{{ route('cookies.policy') }}">Cookies Policy</a><span class="ms-2">|</span></li>
						<li class="list-inline-item @if(request()->is('returns-policy')) active @endif "><a href="{{ route('returns.policy') }}">Returns Policy</a><span class="ms-2">|</span></li>
						<li class="list-inline-item @if(request()->is('user.manualEn')) active @endif ">
							@if($language_id == 1)
								<a href="{{ route('user.manualEn') }}">User Manual</a>
							@else
								<a href="{{ route('user.manualDe') }}">User Manual</a>
							@endif
							
						</li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<div class="feature-modal modal fade" id="aufwachenModal" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_5",$outPutArray)) ? $outPutArray['meditation_5'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("wakeup_detail",$outPutArray)) ? $outPutArray['wakeup_detail'] :  '' !!}</p>
					<div class="row">
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} I</h4>
							<img src="{{ asset('public/assets/frontend/img/set-alarm.png')}}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("wakeup_step_1_detail",$outPutArray)) ? $outPutArray['wakeup_step_1_detail'] :  '' !!}</p>
						</div>
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} II</h4>
							<img src="{{ asset('public/assets/frontend/img/select-day-music.png') }}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("wakeup_step_2_detail",$outPutArray)) ? $outPutArray['wakeup_step_2_detail'] :  '' !!}</p>
						</div>
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} III</h4>
							<img src="{{ asset('public/assets/frontend/img/select-sunrise.png')}}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("wakeup_step_3_detail",$outPutArray)) ? $outPutArray['wakeup_step_3_detail'] :  '' !!}</p>
						</div>
					</div>
					<h5 class="h3 fw-bold fw-bold text-primary mt-4">{!! ($outPutArray && array_key_exists("wakeup_desc_title",$outPutArray)) ? $outPutArray['wakeup_desc_title'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("wakeup_desc",$outPutArray)) ? $outPutArray['wakeup_desc'] :  '' !!}</p>
					<div class="row justify-content-center">
						<div class="col-lg-7">
							<div class="row">
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp-aufwachenModal" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/the-app.png')}}" alt="Die App" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("the_app",$outPutArray)) ? $outPutArray['the_app'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/advantages.png') }}" alt="Vorteile" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("advantages",$outPutArray)) ? $outPutArray['advantages'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/science.png')}}" alt="Wissenschaft" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("science",$outPutArray)) ? $outPutArray['science'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/sun-phases.png')}}" alt="Sonnenphasen" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("solar_phases",$outPutArray)) ? $outPutArray['solar_phases'] :  '' !!}</p>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="modal-footer">
              <button class="btn btn-primary" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">Back to first</button>
            </div> -->
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="theApp-aufwachenModal" aria-hidden="true" tabindex="-1" data-bs-backdrop="static">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-back-aufwachenModal btn btn-outline-primary" data-bs-dismiss="modal" aria-label="Close">{!! ($outPutArray && array_key_exists("back",$outPutArray)) ? $outPutArray['back'] :  '' !!}</button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_5",$outPutArray)) ? $outPutArray['meditation_5'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("wakeup_detail",$outPutArray)) ? $outPutArray['wakeup_detail'] :  '' !!}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="einfschlafenModal" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_3",$outPutArray)) ? $outPutArray['meditation_3'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("fall_asleep_detail",$outPutArray)) ? $outPutArray['fall_asleep_detail'] :  '' !!}</p>
					<div class="row">
						<div class="col-md-4 d-flex flex-column mb-3 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} I</h4>
							<img src="{{ asset('public/assets/frontend/img/sleep-modal-01.png') }}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("fall_asleep_step_1_detail",$outPutArray)) ? $outPutArray['fall_asleep_step_1_detail'] :  '' !!}</p>
						</div>
						<div class="col-md-4 d-flex flex-column mb-3 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} II</h4>
							<img src="{{ asset('public/assets/frontend/img/sleep-modal-02.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
						<div class="col-md-4 d-flex flex-column mb-3 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} III</h4>
							<img src="{{ asset('public/assets/frontend/img/sleep-modal-03.png') }}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("fall_asleep_step_3_detail",$outPutArray)) ? $outPutArray['fall_asleep_step_3_detail'] :  '' !!}</p>
						</div>
					</div>
					<h5 class="h3 fw-bold fw-bold text-primary mt-4">{!! ($outPutArray && array_key_exists("fall_asleep_desc_title",$outPutArray)) ? $outPutArray['fall_asleep_desc_title'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("fall_asleep_desc",$outPutArray)) ? $outPutArray['fall_asleep_desc'] :  '' !!}</p>
					<div class="row justify-content-center">
						<div class="col-lg-7">
							<div class="row">
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp-einfschlafenModal" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/the-app.png')}}" alt="Die App" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("the_app",$outPutArray)) ? $outPutArray['the_app'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/advantages.png')}}" alt="Vorteile" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("advantages",$outPutArray)) ? $outPutArray['advantages'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/science.png')}}" alt="Wissenschaft" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("science",$outPutArray)) ? $outPutArray['science'] :  '' !!}</p>
									</button>
								</div>
								<div class="col-6 col-md-3 text-center">
									<button data-bs-target="#theApp" class="icons btn" data-bs-toggle="modal">
										<img src="{{ asset('public/assets/frontend/img/sun-phases.png')}}" alt="Sonnenphasen" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("solar_phases",$outPutArray)) ? $outPutArray['solar_phases'] :  '' !!}</p>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="theApp-einfschlafenModal" aria-hidden="true" tabindex="-1" data-bs-backdrop="static">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-back-einfschlafenModal btn btn-outline-primary" data-bs-dismiss="modal" aria-label="Close">{!! ($outPutArray && array_key_exists("back",$outPutArray)) ? $outPutArray['back'] :  '' !!}</button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_3",$outPutArray)) ? $outPutArray['meditation_3'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("fall_asleep_app_detail",$outPutArray)) ? $outPutArray['fall_asleep_app_detail'] :  '' !!}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="lichtModal" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_4",$outPutArray)) ? $outPutArray['meditation_4'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("light_detail_1",$outPutArray)) ? $outPutArray['light_detail_1'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("light_detail_2",$outPutArray)) ? $outPutArray['light_detail_2'] :  '' !!}</p>
					<div class="row mb-4">
						<div class="col-md-6 d-flex flex-column mb-3 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} I</h4>
							<img src="{{ asset('public/assets/frontend/img/lamp-color.png')}}" alt="" class="img-fluid mx-auto my-auto">
						</div>
						<div class="col-md-6 d-flex flex-column mb-3 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} II</h4>
							<img src="{{ asset('public/assets/frontend/img/select-lamp-color.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
					</div>
					<p>{!! ($outPutArray && array_key_exists("light_desc",$outPutArray)) ? $outPutArray['light_desc'] :  '' !!}</p>
				</div>
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="heilungModal" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_1",$outPutArray)) ? $outPutArray['meditation_1'] :  '' !!}</h5>
					<p>{!! ($outPutArray && array_key_exists("healing_detail",$outPutArray)) ? $outPutArray['healing_detail'] :  '' !!}</p>
					<div class="row mb-4">
						<div class="col-md-6 d-flex flex-column mb-0 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} I</h4>
							<img src="{{ asset('public/assets/frontend/img/heilung-modal-01.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
						<div class="col-md-6 d-flex flex-column mb-0 mb-lg-0">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} II</h4>
							<img src="{{ asset('public/assets/frontend/img/heilung-modal-02.png') }}" alt="" class="img-fluid mx-auto my-auto">
							<p class="mb-0 mt-3 small">{!! ($outPutArray && array_key_exists("healing_step_2_detail",$outPutArray)) ? $outPutArray['healing_step_2_detail'] :  '' !!}</p>
						</div>
					</div>
					<p>{!! ($outPutArray && array_key_exists("healing_desc_1",$outPutArray)) ? $outPutArray['healing_desc_1'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("healing_desc_2",$outPutArray)) ? $outPutArray['healing_desc_2'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("healing_desc_3",$outPutArray)) ? $outPutArray['healing_desc_3'] :  '' !!}</p>
					<!-- <div class="row justify-content-center">
						<div class="col-lg-7">
							<div class="row justify-content-center">
								<div class="col-6 col-md-3 text-center">
									<button class="icons btn">
										<img src="{{ asset('public/assets/frontend/img/science.png')}}" alt="Wissenschaft" class="img-fluid">
										<p class="text-primary">{!! ($outPutArray && array_key_exists("science",$outPutArray)) ? $outPutArray['science'] :  '' !!}</p>
									</button>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="feature-modal modal fade" id="meditationModal" aria-hidden="true" tabindex="-1">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="h4 modal-title fw-bold text-primary">SAMINA Sound Light Sleepsystem</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body text-center">
					<h5 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("meditation_2",$outPutArray)) ? $outPutArray['meditation_2'] :  '' !!}</h5>
					<div class="row justify-content-center">
						<div class="col-6 col-md-2 text-center">
							<button class="icons btn">
								<img src="{{ asset('public/assets/frontend/img/science.png')}}" alt="Wissenschaft" class="img-fluid">
								<p class="text-primary">{!! ($outPutArray && array_key_exists("science",$outPutArray)) ? $outPutArray['science'] :  '' !!}</p>
							</button>
						</div>
					</div>
					<p>{!! ($outPutArray && array_key_exists("meditation_detail_1",$outPutArray)) ? $outPutArray['meditation_detail_1'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("meditation_detail_2",$outPutArray)) ? $outPutArray['meditation_detail_2'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("meditation_detail_3",$outPutArray)) ? $outPutArray['meditation_detail_3'] :  '' !!}</p>
					<p>{!! ($outPutArray && array_key_exists("meditation_detail_4",$outPutArray)) ? $outPutArray['meditation_detail_4'] :  '' !!}</p>
					<div class="row mb-4">
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} I</h4>
							<img src="{{ asset('public/assets/frontend/img/meditation-modal-01.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} II</h4>
							<img src="{{ asset('public/assets/frontend/img/meditation-modal-02.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
						<div class="col-md-4 d-flex flex-column">
							<h4 class="h4 fw-bold p-2 step-title">{!! ($outPutArray && array_key_exists("step",$outPutArray)) ? $outPutArray['step'] :  '' !!} III</h4>
							<img src="{{ asset('public/assets/frontend/img/meditation-modal-03.png') }}" alt="" class="img-fluid mx-auto my-auto">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Component Section Modal -->
	<div class="modal fade" id="componentPillowModal" tabindex="-1" aria-labelledby="componentPillowModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-xl">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="componentPillowModalLabel">{!! ($outPutArray && array_key_exists("delta_pillow_modal_title",$outPutArray)) ? $outPutArray['delta_pillow_modal_title'] :  '' !!}</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="ratio ratio-16x9 mb-4">
					<iframe src="https://www.youtube.com/embed/wSEKrv5_W2A" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<div>{!! ($outPutArray && array_key_exists("delta_pillow_modal_detail_1",$outPutArray)) ? $outPutArray['delta_pillow_modal_detail_1'] :  '' !!}</div>
				<img alt="" src="{{ asset('public/assets/frontend/img/Delta-Evo.jpg') }}" class="img-fluid mb-3" />
				<div>{!! ($outPutArray && array_key_exists("delta_pillow_modal_detail_2",$outPutArray)) ? $outPutArray['delta_pillow_modal_detail_2'] :  '' !!}</div>
				<img alt="" src="{{ asset('public/assets/frontend/img/DeltaEvo-Anwendungsbeispiele-3.jpg') }}" class="img-fluid mb-3" />
				<div>{!! ($outPutArray && array_key_exists("delta_pillow_modal_detail_3",$outPutArray)) ? $outPutArray['delta_pillow_modal_detail_3'] :  '' !!}</div>
			</div>
			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div> -->
			</div>
		</div>
	</div>
	<!-- / Component Section Modal -->

	<script src="{{ asset('public/assets/frontend/js/jquery-3.6.0.min.js') }}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js"></script>
	<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
	<script src="{{ asset('public/assets/frontend/js/scrollreveal.js') }}"></script>
	<script src="{{ asset('public/assets/frontend/js/circle.js') }}"></script>
	<script>
		var music_img = "{{ asset('public/assets/frontend/img/01.mp3') }}";
		/*----------------
			LOADER
		-----------------*/
		document.onreadystatechange = function () {
			if (document.readyState !== "complete") {
				document.querySelector("body").style.visibility = "hidden";
				document.querySelector("#loader").style.visibility = "visible";
			} else {
				document.querySelector("#loader").style.display = "none";
				document.querySelector("body").style.visibility = "visible";
			}
		};
	</script>
	@if(request()->is('/') || request()->is('index'))
		<script src="{{ asset('public/assets/frontend/js/main.js') }}"></script>
	@endif
	@yield('script')
	<script>
		$(document).ready(function() {
			var value = $('.selectlanaguePicker').find(":selected").val();
			$.cookie("select_language_id", value);
			$('body').on('change', '.selectlanaguePicker', function() {
				var tempValue = $('.selectlanaguePicker').find(":selected").val();
				$.cookie("select_language_id", tempValue);
				location.reload();
			});
		});
	</script>

	<!-- readmore and less script -->
	<script>
		$(document).ready(function() {
			function AddReadMore() {
				var carLmt = 100;
				//var readMoreTxt = " ...{!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}";
				//var readLessTxt = " {!! ($outPutArray && array_key_exists("read_less",$outPutArray)) ? $outPutArray['read_less'] :  '' !!}";

				var readMoreTxt = " ...<i class='fas fa-chevron-down'></i>";
				var readLessTxt = " <i class='fas fa-chevron-up'></i>";
				
				$(".addReadMore").each(function() {
					if ($(this).find(".firstSec").length)
						return;
					var allstr = $(this).text();
					if (allstr.length > carLmt) {
						var firstSet = allstr.substring(0, carLmt);
						var secdHalf = allstr.substring(carLmt, allstr.length);
						var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
						$(this).html(strtoadd);
					}
				});
				$(document).on("click", ".readMore,.readLess", function() {
					$(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
				});
			}
			function AddReadMorelarge() {
				var carLmtlarge = 260;
				//var readMoreTxt = " ...{!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}";
				//var readLessTxt = " {!! ($outPutArray && array_key_exists("read_less",$outPutArray)) ? $outPutArray['read_less'] :  '' !!}";

				var readMoreTxt = " ...<i class='fas fa-chevron-down'></i>";
				var readLessTxt = " <i class='fas fa-chevron-up'></i>";
				
				$(".addReadMorelarge").each(function() {
					if ($(this).find(".firstSec").length)
						return;
					var allstr = $(this).text();
					if (allstr.length > carLmtlarge) {
						var firstSet = allstr.substring(0, carLmtlarge);
						var secdHalf = allstr.substring(carLmtlarge, allstr.length);
						var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span><span class='readMore'  title='Click to Show More'>" + readMoreTxt + "</span><span class='readLess' title='Click to Show Less'>" + readLessTxt + "</span>";
						$(this).html(strtoadd);
					}
				});
				$(document).on("click", ".readMore,.readLess", function() {
					$(this).closest(".addReadMorelarge").toggleClass("showlesscontent showmorecontent");
				});
			}
			if ($(window).width() < 561) {
				AddReadMore();
				AddReadMorelarge();
			}


			/* modal close stop Iframe video */
			$("#componentPillowModal").on('hidden.bs.modal', function (e) {
				$("#componentPillowModal iframe").attr("src", $("#componentPillowModal iframe").attr("src"));
			});
		});
	</script>
	<script>
		$(".showmorebtn span").on("click", function() {
			var $this = $(this); 
			var $content = $this.parent().prev("div.showmorehidecontent");
			var linkText = $this.text().toUpperCase();    
			
			if(linkText === "SHOW MORE"){
				linkText = "Show less";
				$content.switchClass("hideContent", "showContent", 400);
				$this.addClass("showless");
			} else {
				linkText = "Show more";
				$content.switchClass("showContent", "hideContent", 400);
				$this.removeClass("showless");
			};

			$this.text(linkText);
		});
	</script>
</body>

</html>