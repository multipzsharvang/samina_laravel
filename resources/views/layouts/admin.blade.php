<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Samina') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/backend/images/favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('public/assets/backend/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/libs/owl.carousel/assets/owl.theme.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/backend/libs/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/backend/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/backend/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
</head>
<body data-sidebar="dark">
    <!-- Begin page -->
    <div id="app">
        @include("partials.topbar")
        @include("partials.sidebar")
        <section class="main-content">
            @yield('content')
        </section>
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Samina.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-right d-none d-sm-block">
                            Design & Develop by <a href="https://www.applie.li" target="_blank">Applie AG</a>.
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- END layout-wrapper -->
    <!-- JAVASCRIPT -->
    <script src="{{ asset('public/assets/backend/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/owl.carousel/owl.carousel.min.js') }}"></script>
    <!--<script src="{{ asset('public/assets/backend/js/pages/dashboard.init.js') }}"></script>-->
    <script src="{{ asset('public/assets/backend/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/assets/backend/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <!-- Swwtalert Js -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!--<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>-->
    <script src="{{ asset('public/assets/backend/libs/ckeditor/ckeditor.js') }}"></script>
    <!-- App js -->
    <script src="{{ asset('public/assets/backend/js/app.js') }}"></script>
    <script src="{{ asset('public/assets/backend/js/custom.js') }}"></script>
    <!-- JAVASCRIPT -->
    @yield('scripts')
    @php
        $routeName = Route::currentRouteName();
        $route = str_replace(".index","",$routeName);
        $all_routes = array("languages","keyword");
    @endphp
    @if(in_array($route, $all_routes))
    <script>
        $(function() {
            var errorMsg = "{{trans('global.something_wrong')}}";
            var _token = $('meta[name="csrf-token"]').attr('content');
            var table = $('.datatable-User').DataTable();
            $('body').on('click', '.switch_checkbox_btn', function() {
                var id = $(this).attr('entry-id');
                jQuery('.main-loader').show();
                $.ajax({
                        headers: {
                            'x-csrf-token': _token
                        },
                        method: 'POST',
                        url: "{{ route($route.'.store') }}",
                        data: {
                            id: id,
                            _method: 'POST'
                        }
                    })
                    .done(function(data) {
                        if(data.error){
                            Swal.fire("Error!", data.message,
                                    "danger");
                        }
                        jQuery('.main-loader').hide();
                    })
            });
            $('body').on('click', '.action-delete', function() {
                var current = $(this);
                var id = $(this).attr('entry-id');
                Swal.fire({
                    icon: 'warning',
                    title: "Are you sure delete this?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Yes, delete it!",
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                                headers: {
                                    'x-csrf-token': _token
                                },
                                method: 'POST',
                                url: "{{ route($route.'.destroy'," + id + ") }}",
                                data: {
                                    id: id,
                                    _method: 'DELETE'
                                }
                            })
                            .done(function(data) {
                                if(data.error){
                                    Swal.fire("Error!", data.message,
                                    "danger");
                                }else if(data.success){
                                    Swal.fire("Deleted!", "Successfully.",
                                    "success");
                                    table.row(current.parents('tr'))
                                        .remove()
                                        .draw();
                                }
                            })
                    }
                });
            });
        });
    </script>
    @endif
</body>
</html>