 <!-- end page title -->
 @if(session('success'))
     <div class="row mb-2">
         <div class="col-lg-12">
             <div class="alert alert-success" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="X">
                     <span aria-hidden="true">&times;</span>
                 </button>
                 {{ session('success') }}
             </div>
         </div>
     </div>
@endif
@if(session('danger'))
     <div class="row mb-2">
         <div class="col-lg-12">
             <div class="alert alert-danger" role="alert">
                 {{ session('danger') }}
             </div>
         </div>
     </div>
@endif
@if ($errors->any())
    <div class="row mb-2">
         <div class="col-lg-12">
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        </div>
    </div>
@endif
