    <aside class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <!-- Admin show menu start -->


                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">{{ trans('cruds.menu') }}</li>
                    <li class="{{ request()->is('languages') || request()->is('languages/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('languages.index') }}" class="waves-effect {{ request()->is('languages') || request()->is('languages/*') ? 'active' : '' }}">
                            <span>{{ trans('cruds.languages.title') }}</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('keyword') || request()->is('keyword/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('keyword.index') }}" class="waves-effect {{ request()->is('keyword') || request()->is('keyword/*') ? 'active' : '' }}">
                            <span>{{ trans('cruds.keyword.title') }}</span>
                        </a>
                    </li>
                    <li class="{{ request()->is('content') || request()->is('content/*') ? 'mm-active' : '' }}">
                        <a href="{{ route('content.index') }}" class="waves-effect {{ request()->is('content') || request()->is('content/*') ? 'active' : '' }}">
                            <span>{{ trans('cruds.content.title') }}</span>
                        </a>
                    </li>
                </ul>
                <!-- Admin show menu end -->
            </div>
            <!-- Sidebar -->
        </div>
    </aside>
    <!-- Left Sidebar End -->