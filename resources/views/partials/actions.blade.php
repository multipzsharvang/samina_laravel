<div class="button items">
@php
    $routeName = Route::currentRouteName();
    $route = str_replace(".index","",$routeName);

    $all_routes =array("user","music","transactionhistory");

    $delete_routes = array("keyword","faq","ticketcategory","musictype","artist","category","alarmdurationtime","alarmtype","supportticket","music","therapy","frequencytype");
    
    $edit_routes = array("languages","keyword","faq","ticketcategory","musictype","artist","category","alarmdurationtime","alarmtype","supportticket","content","mobileversion","music","user","therapy","frequencytype");

    $id = encrypt($id);
@endphp

@if(in_array($route, $all_routes))
    <!--<a href="{{ route($route.'.show', $id) }}" title="Show" class="btn btn-sm btn-success">
        <i class="fa fa-eye"></i>
    </a>-->
    <a href="{{ route($route.'.show', $id) }}" class="btn btn-sm btn-icon view rounded-circle" title="View">
        <i class="bx bx-show-alt"></i>
    </a>
@endif

@if(in_array($route, $edit_routes))
    <!--<a href="{{ route($route.'.edit', $id) }}" title="Edit" class="btn btn-sm btn-primary">
        <i class="fa fa-edit"></i>
    </a>-->
    <a href="{{ route($route.'.edit', $id) }}" class="btn btn-sm btn-icon edit rounded-circle" title="Edit">
        <i class="bx bx-edit-alt"></i>
    </a>
@endif

@if(in_array($route, $delete_routes))
    <!--<a href="javascript:{}" title="Delete" class="action-delete btn btn-sm btn-danger" entry-id="{{ $id }}" > 
        <i class="fa fa-trash-alt"></i>
    </a>-->
    
    <a href="javascript:void(0);" class="action-delete btn btn-sm btn-icon delete rounded-circle" title="Delete" entry-id="{{ $id }}">
        <i class="bx bx-trash"></i>
    </a>
@endif
</div>