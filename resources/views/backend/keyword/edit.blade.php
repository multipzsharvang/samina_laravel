@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4>{{ trans('global.update') }} {{ trans('cruds.keyword.title_singular') }}</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('keyword.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <form action="{{ route("keyword.update", [$keyword->id]) }}" method="POST" enctype="multipart/form-data" id="add_form">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="title">{{ trans('cruds.keyword.fields.keyword') }}<span class="required_class">*</span></label>
                                <input type="text" class="form-control" id="keyword" name="keyword" value="{{ old('name', isset($keyword->keyword) ? $keyword->keyword : '') }}" required="" readonly="" />
                            </div>

                            @foreach($language as $key1 => $languages)
                            <div class="form-group">
                                <label for="title">{{ $languages->name }}<span class="required_class">*</span></label>
                                <!-- <input type="text" class="form-control"  name="language_{{ $languages->code }}" 
                                value="{{ (!$keyword->multiple_keyword_type_name->isEmpty() && $keyword->multiple_keyword_type_name) ? $keyword->multiple_keyword_type_name[$key1]->keyword_language  :  '' }}" required="" /> -->

                               

                                <textarea style="height:500px" class="ckeditor  form-control" name="language_{{ $languages->code }}"  required="" >{{ (!$keyword->multiple_keyword_type_name->isEmpty() && $keyword->multiple_keyword_type_name) ? $keyword->multiple_keyword_type_name[$key1]->keyword_language  :  '' }}</textarea>

                            </div>
                            @endforeach
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection