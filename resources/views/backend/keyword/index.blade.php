@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h4>{{ trans('cruds.keyword.title_singular') }} {{ trans('global.list') }}</h4>
            </div>

            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('keyword.create') }}" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        {{ trans('global.add') }} {{ trans('cruds.keyword.title_singular') }}
                    </a>
                </div>
            </div>
           
            <div class="col-md-12 mt-3">
               <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <div class="table-responsive">
                            <table class="data-table table table-bordered table-striped table-hover datatable datatable-User">
                                <thead>
                                    <tr>
                                        <th>{{ trans('cruds.keyword.fields.id') }}</th>
                                        <th>{{ trans('cruds.keyword.fields.keyword') }}</th>
                                        @foreach($language as $languages)
                                        <th>
                                            {{ ucfirst($languages->name) }} 
                                        </th>      
                                        @endforeach
                                        <th>{{ trans('global.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($keyword as $key => $value)
                                    <tr data-entry-id="{{ $value->id }}">
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->keyword ?? ''}}</td>
                                        @foreach($language as $key1 => $languages)
                                        <td>
                                            {!! (!$value->multiple_keyword_type_name->isEmpty() && $value->multiple_keyword_type_name) ? $value->multiple_keyword_type_name[$key1]->keyword_language  : '-' !!}
                                        </td>      
                                        @endforeach
                                        <td>@include('partials.actions', ['id' => $value->id])</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
