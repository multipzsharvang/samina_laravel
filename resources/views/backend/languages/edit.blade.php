@extends('layouts.admin')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4>{{ trans('global.update') }} {{ trans('cruds.languages.title_singular') }}</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="float-right">
                    <a href="{{ route('languages.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include("partials.alert")
                        <form action="{{ route('languages.store') }}" method="POST" enctype="multipart/form-data" id="add_form">
                            @csrf
                            <input type="hidden" name="id" value="{{encrypt($languages->id)}}">
                            <div class="form-group">
                                <label for="title">{{ trans('cruds.languages.fields.name') }}<span class="required_class">*</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', isset($languages->name) ? $languages->name : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="title">{{ trans('cruds.languages.fields.code') }}<span class="required_class">*</span></label>
                                <input type="text" class="form-control" id="code" name="code" value="{{ old('name', isset($languages->code) ? $languages->code : '') }}">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>    
@endsection
@section('scripts')
<script type="text/javascript">
    $(function() {
        $("#add_car_type_form").validate({
            // Specify validation rules       
            ignore: [],
            rules: {
                name: {
                    required: true
                },
            },
            // Specify validation error messages
            messages: {
                name: {
                    required: "@lang('validation.required',['attribute'=>'Name'])",
                }
            },

            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
@endsection