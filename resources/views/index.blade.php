@extends('layouts.front_end')
@section('content')
<section class="hero-wrap">
    <div class="leaf leaf-1">
        <img src="{{ asset('public/assets/frontend/img/leaf-1.png') }}" alt="" class="img-fluid thumbnail">
    </div>
    <div class="leaf leaf-2">
        <img src="{{ asset('public/assets/frontend/img/leaf-2.png') }}" alt="" class="img-fluid thumbnail">
    </div>
    <div class="leaf leaf-3">
        <img src="{{ asset('public/assets/frontend/img/leaf-3.png') }}" alt="" class="img-fluid thumbnail">
    </div>
    <div class="leaf leaf-4">
        <img src="{{ asset('public/assets/frontend/img/leaf-4.png') }}" alt="" class="img-fluid thumbnail">
    </div>
    <div class="hero-sec-1">
        <h1 class="main-title text-center mt-md-5">
            {!! ($outPutArray && array_key_exists("label_1",$outPutArray)) ? $outPutArray['label_1'] :  '' !!}
        </h1>
        <img src="{{ asset('public/assets/frontend/img/hero-product.png') }}" alt="speaker" class="img-fluid main-product">
        <div class="hero-detail">
            <h2 class="h3 text-primary text-center fade-up">
                {!! ($outPutArray && array_key_exists("label_2",$outPutArray)) ? $outPutArray['label_2'] :  '' !!}
            </h2>
            <a href="#pillow" class="btn-discover">
                <img src="{{ asset('public/assets/frontend/img/btn-discover.png') }}" alt="">
                <span>{!! ($outPutArray && array_key_exists("btn_discover",$outPutArray)) ? $outPutArray['btn_discover'] :  '' !!}</span>
            </a>
        </div>
    </div>
    <div class="hero-sec-2">
        <div class="cloud-01"></div>
    </div>
</section>
<main style="background: url({{ asset('public/assets/frontend/img/wood-pattern.png') }})repeat center top / 100% 5%">
    <section id="pillow" class="py-3 py-md-5">
        <div class="container text-center secondintrotext">
            
            <div class="showmorehidecontent hideContent">
                <p>{!! ($outPutArray && array_key_exists("pillow_1",$outPutArray)) ? $outPutArray['pillow_1'] :  '' !!}</p>
                <!-- <p class="fw-bold">{!! ($outPutArray && array_key_exists("pillow_2",$outPutArray)) ? $outPutArray['pillow_2'] :  '' !!}</p>
                <p>{!! ($outPutArray && array_key_exists("pillow_3",$outPutArray)) ? $outPutArray['pillow_3'] :  '' !!}</p> -->
            </div>
            <div class="showmorebtn"><span>Show more</span>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <img src="{{ asset('public/assets/frontend/img/assembly-cusion.png') }}" alt="" class="img-fluid my-3" style="transform: scale(0.3);">
                </div>
            </div>
        </div>
    </section>
    <section class="min-vh-100 overflow-hidden py-5">
        <div class="container text-center">
            <h4 class="h2 text-primary mb-3 fw-bold">{!! ($outPutArray && array_key_exists("function_1",$outPutArray)) ? $outPutArray['function_1'] :  '' !!}<br />{!! ($outPutArray && array_key_exists("function_1_1",$outPutArray)) ? $outPutArray['function_1_1'] :  '' !!}</h4>
            <p>{!! ($outPutArray && array_key_exists("function_2",$outPutArray)) ? $outPutArray['function_2'] :  '' !!}<br class="d-none d-lg-block" /> {!! ($outPutArray && array_key_exists("function_2_2",$outPutArray)) ? $outPutArray['function_2_2'] :  '' !!}</p>
        </div>
        <section id="dayNightView" class="day-night-view">
            <!-- <img src="{{ asset('public/assets/frontend/img/bg-shape-paper.png') }}" alt="" class="bg-shape-paper"> -->
            <img src="{{ asset('public/assets/frontend/img/cloud-01.png') }}" alt="" class="bg-cloud">
            <div class="carousel-inner" style="clip-path: polygon(20% 0%, 31% 3%, 43% 1%, 55% 3%, 66% 3%, 84% 0%, 94% 3%, 100% 0, 100% 100%, 0 100%, 0 0, 8% 2%);">
                <div class="carousel-item active" data-item-name="day">
                    <img src="{{ asset('public/assets/frontend/img/day-mountain.png') }}" alt="" class="day-mountain img-fluid w-100 position-absolute" style="inset: 0;z-index: 1;">
                    <!-- <img src="{{ asset('public/assets/frontend/img/sun_01.png') }}" alt="" class="sunrise"> -->
                    <img src="{{ asset('public/assets/frontend/img/sun_02.png') }}" alt="" class="sunrise-top">
                    <div class="sun-details p-md-3 p-2 bg-white rounded-2">
                        <h3 class="h3 fw-bold text-primary">{!! ($outPutArray && array_key_exists("day_view_title",$outPutArray)) ? $outPutArray['day_view_title'] :  '' !!}</h3>
                        <p class="mb-0">{!! ($outPutArray && array_key_exists("day_view_desc",$outPutArray)) ? $outPutArray['day_view_desc'] :  '' !!}</p>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/moon.png') }}" alt="" class="moon">
                    <div class="moon-details p-md-3 p-2 bg-white rounded-2">
                        <h3 class="h3 fw-bold text-primary">{!! ($outPutArray && array_key_exists("night_view_title",$outPutArray)) ? $outPutArray['night_view_title'] :  '' !!}</h3>
                        <p class="mb-0">{!! ($outPutArray && array_key_exists("night_view_desc",$outPutArray)) ? $outPutArray['night_view_desc'] :  '' !!}</p>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/meditation.png') }}" class="d-block w-100 meditation" alt="meditation">
                    <div class="meditation-details p-md-3 p-2 bg-white rounded-2">
                        <h3 class="h3 fw-bold text-primary">{!! ($outPutArray && array_key_exists("meditation_view_title",$outPutArray)) ? $outPutArray['meditation_view_title'] :  '' !!}</h3>
                        <p class="mb-0">{!! ($outPutArray && array_key_exists("meditation_view_desc",$outPutArray)) ? $outPutArray['meditation_view_desc'] :  '' !!}</p>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/day.png') }}" class="d-block w-100 day" alt="day">
                    <img src="{{ asset('public/assets/frontend/img/night_glow.png') }}" alt="" class="night-glow">
                    <img src="{{ asset('public/assets/frontend/img/day_glow.png') }}" alt="" class="day-glow">
                    <img src="{{ asset('public/assets/frontend/img/lamp.png') }}" alt="" class="img-fluid img-product">
                </div>
            </div>
        </section>

        <section class="therapy-section py-lg-5 py-3">
        <div class="container text-center">
            <h4 class="h2 text-primary mb-5"><b>{!! ($outPutArray && array_key_exists("therapy_title",$outPutArray)) ? $outPutArray['therapy_title'] :  '' !!}</b><br class="d-none d-lg-block">{!! ($outPutArray && array_key_exists("therapy_title_1",$outPutArray)) ? $outPutArray['therapy_title_1'] :  '' !!}</h4>
        </div>
        <div id="therapy" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="false">
            <div class="carousel-inner pb-4">
                <div class="carousel-item active">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #D9D9D9;"></div>
                        <img src="{{ asset('public/assets/frontend/img/white.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/white/general-healing.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("white_img_1",$outPutArray)) ? $outPutArray['white_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/white/pulmonary-circulation.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("white_img_2",$outPutArray)) ? $outPutArray['white_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/white/depression.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("white_img_3",$outPutArray)) ? $outPutArray['white_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/white/exhaustion.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("white_img_4",$outPutArray)) ? $outPutArray['white_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("white_title",$outPutArray)) ? $outPutArray['white_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("white_sub_title",$outPutArray)) ? $outPutArray['white_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("white_desc",$outPutArray)) ? $outPutArray['white_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/white-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #FF2323;"></div>
                        <img src="{{ asset('public/assets/frontend/img/Red.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/red/depression.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("red_img_1",$outPutArray)) ? $outPutArray['red_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/red/digestive-disorders.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("red_img_2",$outPutArray)) ? $outPutArray['red_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/red/feeling-of-weakness.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("red_img_3",$outPutArray)) ? $outPutArray['red_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/red/low-blood-pressure.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("red_img_4",$outPutArray)) ? $outPutArray['red_img_4'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/red/wound-healing.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("red_img_5",$outPutArray)) ? $outPutArray['red_img_5'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("red_title",$outPutArray)) ? $outPutArray['red_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("red_sub_title",$outPutArray)) ? $outPutArray['red_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("red_desc",$outPutArray)) ? $outPutArray['red_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/red-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #9000FF;"></div>
                        <img src="{{ asset('public/assets/frontend/img/Purple.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/purple/meditation.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("violet_img_1",$outPutArray)) ? $outPutArray['violet_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/purple/nerve-damage.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("violet_img_2",$outPutArray)) ? $outPutArray['violet_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/purple/relieve-cramps.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("violet_img_3",$outPutArray)) ? $outPutArray['violet_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/purple/strengthen-the-immune-and-lymphatic-system.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("violet_img_4",$outPutArray)) ? $outPutArray['violet_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("violet_title",$outPutArray)) ? $outPutArray['violet_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("violet_sub_title",$outPutArray)) ? $outPutArray['violet_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("violet_desc",$outPutArray)) ? $outPutArray['violet_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/purple-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item ">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #43DC3F;"></div>
                        <img src="{{ asset('public/assets/frontend/img/green.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/green/exhaustion.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("green_img_1",$outPutArray)) ? $outPutArray['green_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/green/heart-pain.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("green_img_2",$outPutArray)) ? $outPutArray['green_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/green/high-blood-pressure.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("green_img_3",$outPutArray)) ? $outPutArray['green_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/green/tumors.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("green_img_4",$outPutArray)) ? $outPutArray['green_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("green_title",$outPutArray)) ? $outPutArray['green_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("green_sub_title",$outPutArray)) ? $outPutArray['green_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("green_desc",$outPutArray)) ? $outPutArray['green_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/green-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #2235FE;"></div>
                        <img src="{{ asset('public/assets/frontend/img/blue.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/blue/lower-the-pulse.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("blue_img_1",$outPutArray)) ? $outPutArray['blue_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/blue/relieve-tention.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("blue_img_2",$outPutArray)) ? $outPutArray['blue_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/blue/sleep-disorders.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("blue_img_3",$outPutArray)) ? $outPutArray['blue_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/blue/stop-pain.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("blue_img_4",$outPutArray)) ? $outPutArray['blue_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("blue_title",$outPutArray)) ? $outPutArray['blue_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("blue_sub_title",$outPutArray)) ? $outPutArray['blue_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("blue_desc",$outPutArray)) ? $outPutArray['blue_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/blue-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #BB4E9F;"></div>
                        <img src="{{ asset('public/assets/frontend/img/Pink.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/pink/anxiety.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("pink_img_1",$outPutArray)) ? $outPutArray['pink_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/pink/chronic-illnesses.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("pink_img_2",$outPutArray)) ? $outPutArray['pink_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/pink/heart-disease.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("pink_img_3",$outPutArray)) ? $outPutArray['pink_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/pink/mental-problem.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("pink_img_4",$outPutArray)) ? $outPutArray['pink_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("pink_title",$outPutArray)) ? $outPutArray['pink_title'] :  '' !!} </h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("pink_sub_title",$outPutArray)) ? $outPutArray['pink_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("pink_desc",$outPutArray)) ? $outPutArray['pink_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/pink-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #AFB037;"></div>
                        <img src="{{ asset('public/assets/frontend/img/Yellow.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/yellow/asthma.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("yellow_img_1",$outPutArray)) ? $outPutArray['yellow_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/yellow/diabetes.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("yellow_img_2",$outPutArray)) ? $outPutArray['yellow_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/yellow/paralysis.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("yellow_img_3",$outPutArray)) ? $outPutArray['yellow_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/yellow/tention.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("yellow_img_4",$outPutArray)) ? $outPutArray['yellow_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("yellow_title",$outPutArray)) ? $outPutArray['yellow_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("yellow_sub_title",$outPutArray)) ? $outPutArray['yellow_sub_title'] :  '' !!} </p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("yellow_desc",$outPutArray)) ? $outPutArray['yellow_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/yellow-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
                <div class="carousel-item">
                    <figure class="bg-therapy">
                        <div class="ovarlay" style="background-color: #F08844;"></div>
                        <img src="{{ asset('public/assets/frontend/img/Orange.png') }}" alt="" class="img-fluid d-block">
                    </figure>
                    <div class="container-fluid" style="width: 90vw;">
                        <div class="row align-items-center">
                            <div class="col-xl-auto mb-4 mb-lg-0">
                                <p class="text-primary fw-bold text-center mb-2 mb-md-4">{!! ($outPutArray && array_key_exists("therapy_ideal_title",$outPutArray)) ? $outPutArray['therapy_ideal_title'] :  '' !!}</p>
                                <div class="therapy-detail-wrap">
                                    <div class="row therapy-icons">
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/orange/depression.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("orange_img_1",$outPutArray)) ? $outPutArray['orange_img_1'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/orange/kidney-disease.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("orange_img_2",$outPutArray)) ? $outPutArray['orange_img_2'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/orange/lack-of-energy.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("orange_img_3",$outPutArray)) ? $outPutArray['orange_img_3'] :  '' !!}</span>
                                        </div>
                                        <div class="col">
                                            <img src="{{ asset('public/assets/frontend/img/orange/muscle-cramps.png') }}" alt="" class="d-block mx-auto img-fluid">
                                            <span class="fs-6 d-block text-center mt-2">{!! ($outPutArray && array_key_exists("orange_img_4",$outPutArray)) ? $outPutArray['orange_img_4'] :  '' !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl text-center">
                                <div class="therapy-detail-wrap">
                                    <h2 class="h2 fw-bold text-primary">{!! ($outPutArray && array_key_exists("orange_title",$outPutArray)) ? $outPutArray['orange_title'] :  '' !!}</h2>
                                    <p class="text-primary fw-bold">{!! ($outPutArray && array_key_exists("orange_sub_title",$outPutArray)) ? $outPutArray['orange_sub_title'] :  '' !!}</p>
                                    <p class="therapy-detail addReadMore showlesscontent">{!! ($outPutArray && array_key_exists("orange_desc",$outPutArray)) ? $outPutArray['orange_desc'] :  '' !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('public/assets/frontend/img/orange-hand.png') }}" alt="" class="img-fluid therapy-mobile-screen d-none d-xl-block">
                </div>
            </div>
            <div class="carousel-indicators">
                <div data-bs-target="#therapy" data-bs-slide-to="0" class="active" aria-label="Slide 4">
                    <img src="{{ asset('public/assets/frontend/img/icons/white.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="1" aria-label="Slide 2">
                    <img src="{{ asset('public/assets/frontend/img/icons/red.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="2" aria-label="Slide 3">
                    <img src="{{ asset('public/assets/frontend/img/icons/purple.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="3" aria-current="true" aria-label="Slide 1">
                    <img src="{{ asset('public/assets/frontend/img/icons/green.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="4" aria-label="Slide 5">
                    <img src="{{ asset('public/assets/frontend/img/icons/blue.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="5" aria-label="Slide 6">
                    <img src="{{ asset('public/assets/frontend/img/icons/pink.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="6" aria-label="Slide 7">
                    <img src="{{ asset('public/assets/frontend/img/icons/yellow.png') }}" alt="" class="color-bullet">
                </div>
                <div data-bs-target="#therapy" data-bs-slide-to="7" aria-label="Slide 8">
                    <img src="{{ asset('public/assets/frontend/img/icons/orange.png') }}" alt="" class="color-bullet">
                </div>
            </div>
            <button class="carousel-control-prev d-block d-md-none" type="button" data-bs-target="#therapy" data-bs-slide="prev">
                <span><i class="fas fa-chevron-left" aria-hidden="true"></i></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next d-block d-md-none" type="button" data-bs-target="#therapy" data-bs-slide="next">
                <span><i class="fas fa-chevron-right" aria-hidden="true"></i></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </section>

        <div class="holderCircle">
            <div class="dotCircle">
                <span class="itemDot active itemDot1" data-tab="1">
                    <button class="text-white btn" data-bs-toggle="modal" data-bs-target="#heilungModal" role="button">
                        <img src="{{ asset('public/assets/frontend/img/Healing.png') }}" alt="">
                        <h5 class="h4 fw-bold heading d-none d-md-block">{!! ($outPutArray && array_key_exists("meditation_1",$outPutArray)) ? $outPutArray['meditation_1'] :  '' !!}</h5>
                        <span class="forActive"></span>
                    </button>
                </span>
                <span class="itemDot itemDot2" data-tab="2">
                    <button class="text-white btn" data-bs-toggle="modal" data-bs-target="#meditationModal" role="button">
                        <img src="{{ asset('public/assets/frontend/img/Meditation-circle.png') }}" alt="">
                        <h5 class="h4 fw-bold heading d-none d-md-block">{!! ($outPutArray && array_key_exists("meditation_2",$outPutArray)) ? $outPutArray['meditation_2'] :  '' !!}</h5>
                        <span class="forActive"></span>
                    </button>
                </span>
                <span class="itemDot itemDot3" data-tab="3">
                    <button class="text-white btn" data-bs-toggle="modal" data-bs-target="#einfschlafenModal" role="button">
                        <img src="{{ asset('public/assets/frontend/img/Fall-asleep.png') }}" alt="">
                        <h5 class="h4 fw-bold heading d-none d-md-block">{!! ($outPutArray && array_key_exists("meditation_3",$outPutArray)) ? $outPutArray['meditation_3'] :  '' !!}</h5>
                        <span class="forActive"></span>
                    </button>
                </span>
                <span class="itemDot itemDot3" data-tab="4">
                    <button class="text-white btn" data-bs-toggle="modal" data-bs-target="#lichtModal" role="button">
                        <img src="{{ asset('public/assets/frontend/img/Light.png') }}" alt="">
                        <h5 class="h4 fw-bold heading d-none d-md-block">{!! ($outPutArray && array_key_exists("meditation_4",$outPutArray)) ? $outPutArray['meditation_4'] :  '' !!}</h5>
                        <span class="forActive"></span>
                    </button>
                </span>
                <span class="itemDot itemDot3" data-tab="5">
                    <button class="text-white btn" data-bs-toggle="modal" data-bs-target="#aufwachenModal" role="button">
                        <img src="{{ asset('public/assets/frontend/img/Wake-up.png') }}" alt="">
                        <h5 class="h4 fw-bold heading d-none d-md-block">{!! ($outPutArray && array_key_exists("meditation_5",$outPutArray)) ? $outPutArray['meditation_5'] :  '' !!}</h5>
                        <span class="forActive"></span>
                    </button>
                </span>
            </div>
            <div class="contentCircle">
                <div class="CirItem active CirItem1">
                    <a class="text-white" data-bs-toggle="modal" href="#treeOneDetail" role="button">
                        <p class="mb-1">{!! ($outPutArray && array_key_exists("meditation_1",$outPutArray)) ? $outPutArray['meditation_1'] :  '' !!}</p>
                        {!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}
                    </a>
                </div>
                <div class="CirItem CirItem2">
                    <a class="text-white" data-bs-toggle="modal" href="#treeOneDetail" role="button">
                        <p class="mb-1">{!! ($outPutArray && array_key_exists("meditation_2",$outPutArray)) ? $outPutArray['meditation_2'] :  '' !!}</p>
                        {!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}
                    </a>
                </div>
                <div class="CirItem CirItem3">
                    <a class="text-white" data-bs-toggle="modal" href="#treeOneDetail" role="button">
                        <p class="mb-1">{!! ($outPutArray && array_key_exists("meditation_3",$outPutArray)) ? $outPutArray['meditation_3'] :  '' !!}</p>
                        {!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}
                    </a>
                </div>
                <div class="CirItem CirItem4">
                    <a class="text-white" data-bs-toggle="modal" href="#treeOneDetail" role="button">
                        <p class="mb-1">{!! ($outPutArray && array_key_exists("meditation_4",$outPutArray)) ? $outPutArray['meditation_4'] :  '' !!}</p>
                        {!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}
                    </a>
                </div>
                <div class="CirItem CirItem5">
                    <a class="text-white" data-bs-toggle="modal" href="#treeOneDetail" role="button">
                        <p class="mb-1">{!! ($outPutArray && array_key_exists("meditation_5",$outPutArray)) ? $outPutArray['meditation_5'] :  '' !!}</p>
                        {!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section id="cousionPackage" class="py-3 py-md-5">
        <div class="container">
            <h4 class="h2 text-primary mb-3 fw-bold text-center">{!! ($outPutArray && array_key_exists("function_1",$outPutArray)) ? $outPutArray['function_1'] :  '' !!}<br />{!! ($outPutArray && array_key_exists("component",$outPutArray)) ? $outPutArray['component'] :  '' !!}</h4>
            <div class="row my-5 pb-lg-5">
                <div class="col-4 text-center">
                    <a class="d-flex flex-column h-100" href="#pillowWrap" title="{!! ($outPutArray && array_key_exists("component_1",$outPutArray)) ? $outPutArray['component_1'] :  '' !!}">
                        <img src="{{ asset('public/assets/frontend/img/pillow-set.png') }}" alt="" class="img-fluid my-auto img-pillow">
                        <p class="text-primary mb-0">{!! ($outPutArray && array_key_exists("component_1",$outPutArray)) ? $outPutArray['component_1'] :  '' !!}</p>
                    </a>
                </div>
                <div class="col-4 text-center ">
                    <a class="d-flex flex-column h-100" href="#sectionTwo" title="{!! ($outPutArray && array_key_exists("component_2",$outPutArray)) ? $outPutArray['component_2'] :  '' !!}">
                        <div class="my-auto img-lamp-parent">
                            <img src="{{ asset('public/assets/frontend/img/lamp.png') }}" alt="" class="img-fluid my-auto img-lamp">
                        </div>
                        <p class="text-primary mb-0">{!! ($outPutArray && array_key_exists("component_2",$outPutArray)) ? $outPutArray['component_2'] :  '' !!}</p>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="#footer" title="{!! ($outPutArray && array_key_exists("component_3",$outPutArray)) ? $outPutArray['component_3'] :  '' !!}">
                        <img src="{{ asset('public/assets/frontend/img/feature-4.png') }}" alt="" class="img-fluid mb-2 img-mobile">
                        <p class="text-primary mb-0">{!! ($outPutArray && array_key_exists("component_3",$outPutArray)) ? $outPutArray['component_3'] :  '' !!}</p>
                    </a>
                </div>
            </div>
        </div>
        <div id="pillowWrap" class="pillow-wrap" style="background: #f3f3f3 url({{ asset('public/assets/frontend/img/wood-pattern.png') }}) repeat center/100% 50%">
            <div class="container">
                <div class="pillow-package py-md-5">
                    <div class="row justify-content-center pt-lg-4">
                        <div class="col-lg-6 position-relative">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-3.png') }}" alt="pillow" class="img-fluid layer-3">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-2.png') }}" alt="pillow" class="img-fluid layer-2">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-1.png') }}" alt="pillow" class="img-fluid layer-1">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/speaker.png') }}" alt="pillow" class="img-fluid layer-0">
                        </div>
                    </div>
                </div>
                <div class="pillow-detail my-3">
                    <div class="row align-items-center layer-3-detail mb-4">
                        <div class="col-md-3 d-none d-lg-block">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-3.png') }}" alt="" class="img-fluid mb-3 mb-md-0">
                        </div>
                        <div class="col-md-9 text-center text-md-start">
                            <!-- <h4 class="h4 fw-bold text-primary mb-2">Pillow Title</h4> -->
                            <p>{!! ($outPutArray && array_key_exists("component_4",$outPutArray)) ? $outPutArray['component_4'] :  '' !!} <a class="btn btn-outline-primary btn-sm" href="#componentPillowModal" data-bs-toggle="modal">{!! ($outPutArray && array_key_exists("read_more",$outPutArray)) ? $outPutArray['read_more'] :  '' !!}</a></p>
                        </div>
                    </div>
                    <div class="row align-items-center layer-2-detail mb-4">
                        <div class="col-md-3 d-none d-lg-block">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-2.png') }}" alt="" class="img-fluid mb-3 mb-md-0">
                        </div>
                        <div class="col-md-9 text-center text-md-start">
                            <!-- <h4 class="h4 fw-bold text-primary mb-2">Pillow Title</h4> -->
                            <p>{!! ($outPutArray && array_key_exists("component_5",$outPutArray)) ? $outPutArray['component_5'] :  '' !!}</p>
                        </div>
                    </div>
                    <div class="row align-items-center layer-1-detail">
                        <div class="col-md-3 d-none d-lg-block">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/Layer-1.png') }}" alt="" class="img-fluid mb-3 mb-md-0">
                        </div>
                        <div class="col-md-9 text-center text-md-start">
                            <!-- <h4 class="h4 fw-bold text-primary mb-2">Pillow Title</h4> -->
                            <p>{!! ($outPutArray && array_key_exists("component_6",$outPutArray)) ? $outPutArray['component_6'] :  '' !!}</p>
                        </div>
                    </div>
                    <div class="row align-items-center layer-0-detail">
                        <div class="col-md-3 d-none d-lg-block">
                            <img src="{{ asset('public/assets/frontend/img/Pillow/layer-0.png') }}" alt="" class="img-fluid mb-3 mb-md-0 d-block mx-auto" style="max-height: 170px;">
                        </div>
                        <div class="col-md-9 text-center text-md-start">
                            <!-- <h4 class="h4 fw-bold text-primary mb-2">Pillow Title</h4> -->
                            <p>{!! ($outPutArray && array_key_exists("component_7",$outPutArray)) ? $outPutArray['component_7'] :  '' !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="sectionTwo" class="assembly-section pt-3 d-flex align-items-end">
        <img src="{{ asset('public/assets/frontend/img/stand.png') }}" alt="" class="stand">
        <div class="assembly-intro text-center text-primary">
            <h3 class="h2 fw-bold">{!! ($outPutArray && array_key_exists("assemble_title",$outPutArray)) ? $outPutArray['assemble_title'] :  '' !!}</h3>
        </div>
        <div class="container position-relative mb-md-5 panel">
            <div class="row justify-content-center">
                <div class="col-lg-4 pb-4 assembly-elements text-center">
                    <div class="top">
                        <img src="{{ asset('public/assets/frontend/img/top.png') }}" alt="top" class="img-fluid">
                        <div class="description top-detail">{!! ($outPutArray && array_key_exists("assemble_1",$outPutArray)) ? $outPutArray['assemble_1'] :  '' !!}
                            <button type="button" class="btn btn-outline-primary btn-sm" class="points activelink" data-tag="one">{!! ($outPutArray && array_key_exists("learn_more",$outPutArray)) ? $outPutArray['learn_more'] :  '' !!}</button>
                            <a href="javascript:void(0);" class="points activelink" data-tag="one"></a>
                        </div>
                    </div>
                    <div class="circuit-cover">
                        <img src="{{ asset('public/assets/frontend/img/circuit-cover.png') }}" alt="cover" class="img-fluid">
                        <div class="description">{!! ($outPutArray && array_key_exists("assemble_2",$outPutArray)) ? $outPutArray['assemble_2'] :  '' !!}
                            <a href="javascript:void(0);" class="points activelink" data-tag="two"></a>
                        </div>
                    </div>
                    <div class="circuit">
                        <img src="{{ asset('public/assets/frontend/img/circut.png') }}" alt="circuit" class="img-fluid ">
                        <div class="description">{!! ($outPutArray && array_key_exists("assemble_3",$outPutArray)) ? $outPutArray['assemble_3'] :  '' !!}
                            <button type="button" class="btn btn-outline-primary btn-sm" class="points activelink" data-tag="three">{!! ($outPutArray && array_key_exists("learn_more",$outPutArray)) ? $outPutArray['learn_more'] :  '' !!}</button>
                            <a href="javascript:void(0);" class="points activelink" data-tag="three"></a>
                        </div>
                    </div>
                    <div class="base">
                        <img src="{{ asset('public/assets/frontend/img/base.png') }}" alt="Base" class="img-fluid">
                        <div class="description base-detail">{!! ($outPutArray && array_key_exists("assemble_4",$outPutArray)) ? $outPutArray['assemble_4'] :  '' !!}
                            <button type="button" class="btn btn-outline-primary btn-sm" class="points activelink" data-tag="four">{!! ($outPutArray && array_key_exists("learn_more",$outPutArray)) ? $outPutArray['learn_more'] :  '' !!}</button>
                            <a href="javascript:void(0);" class="points activelink" data-tag="four"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="assembly-list mt-lg-5" id="one">
            <button class="rounded-circle btn-outline-primary btn-modal-close">
                <i class="fa fa-close"></i>
            </button>
            <div class="row mx-0 align-items-center">
                <div class="col-12 d-block d-lg-none">
                    <p>{!! ($outPutArray && array_key_exists("assemble_1",$outPutArray)) ? $outPutArray['assemble_1'] :  '' !!}</p>
                </div>
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/02.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_1_1",$outPutArray)) ? $outPutArray['assemble_1_1'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/01.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_1_2",$outPutArray)) ? $outPutArray['assemble_1_2'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/04.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_1_3",$outPutArray)) ? $outPutArray['assemble_1_3'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-end">
                <div class="col-6 col-md-4 order-md-0">
                    <img src="{{ asset('public/assets/frontend/img/top-light.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-6 col-md-4 order-md-2">
                    <img src="{{ asset('public/assets/frontend/img/top-dark.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-12 col-md-4 order-md-1">
                    <p class="text-center">{!! ($outPutArray && array_key_exists("assemble_1_4",$outPutArray)) ? $outPutArray['assemble_1_4'] :  '' !!}</p>
                </div>
            </div>
        </div>
        <div class="assembly-list" id="">content goes here...</div>
        <div class="assembly-list mt-lg-5" id="three">
            <button class="rounded-circle btn-outline-primary btn-modal-close">
                <i class="fa fa-close"></i>
            </button>
            <div class="row mx-0 align-items-center">
                <div class="col-12 d-block d-lg-none">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3",$outPutArray)) ? $outPutArray['assemble_3'] :  '' !!}</p>
                </div>
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/05.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3_1",$outPutArray)) ? $outPutArray['assemble_3_1'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/06.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3_2",$outPutArray)) ? $outPutArray['assemble_3_2'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/07.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3_3",$outPutArray)) ? $outPutArray['assemble_3_3'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/08.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3_4",$outPutArray)) ? $outPutArray['assemble_3_4'] :  '' !!}</p>
                </div>
            </div>
            <div class="row mx-0 align-items-center">
                <div class="col-2">
                    <img src="{{ asset('public/assets/frontend/img/icons/09.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-10">
                    <p>{!! ($outPutArray && array_key_exists("assemble_3_5",$outPutArray)) ? $outPutArray['assemble_3_5'] :  '' !!}</p>
                </div>
            </div>
        </div>
        <div class="assembly-list mt-lg-5" id="four" style="display: grid; place-items: center;">
            <button class="rounded-circle btn-outline-primary btn-modal-close">
                <i class="fa fa-close"></i>
            </button>
            <div class="row mx-0 align-items-center">
                <!-- <img src="{{ asset('public/assets/frontend/img/valsadi-sag.jpg') }}" alt="" class="img-fluid" style="position: absolute; inset:0;z-index:-1;height: 100%;object-fit: cover;"> -->
                <div class="mt-lg-5">
                    <div class="col-12 d-block d-lg-none">
                        <p class="text-center">{!! ($outPutArray && array_key_exists("assemble_4",$outPutArray)) ? $outPutArray['assemble_4'] :  '' !!}</p>
                    </div>
                    <p class="mb-0  mt-lg-5 text-center addReadMorelarge showlesscontent">{!! ($outPutArray && array_key_exists("assemble_4_1",$outPutArray)) ? $outPutArray['assemble_4_1'] :  '' !!}</p>
                </div>
            </div>
        </div>
    </section>

    <section class="features py-lg-5 py-3 " style="background: #f3f3f3 url({{ asset('public/assets/frontend/img/wood-pattern.png') }}) repeat center/100% 50%">
        <div class="d-none d-md-block ratio ratio-16x9 bg-cousion" style="background: url({{ asset('public/assets/frontend/img/lamp-with-cousin.png') }}) no-repeat center/contain"></div>
        <div class="container-fluid">
            <div class="feature-intro text-center text-primary">
                <h3 class="h2 fw-bold d-none d-lg-block">Die Sound Light App</h3>
            </div>
            <div class="fpanel one">
                <div class="row justify-content-center">
                    <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-1.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_home_title",$outPutArray)) ? $outPutArray['app_screen_home_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_home_detail",$outPutArray)) ? $outPutArray['app_screen_home_detail'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_home_feature_intro",$outPutArray)) ? $outPutArray['app_screen_home_feature_intro'] :  '' !!}</p>
                            <ul>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_1",$outPutArray)) ? $outPutArray['app_screen_home_feature_1'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_2",$outPutArray)) ? $outPutArray['app_screen_home_feature_2'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_3",$outPutArray)) ? $outPutArray['app_screen_home_feature_3'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_4",$outPutArray)) ? $outPutArray['app_screen_home_feature_4'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_5",$outPutArray)) ? $outPutArray['app_screen_home_feature_5'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_home_feature_6",$outPutArray)) ? $outPutArray['app_screen_home_feature_6'] :  '' !!}</li>
                            </ul>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fpanel two">
                <div class="row justify-content-center">
                    <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-2.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_colors_title",$outPutArray)) ? $outPutArray['app_screen_colors_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_colors_detail",$outPutArray)) ? $outPutArray['app_screen_colors_detail'] :  '' !!}</p>
                            <ul>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_colors_feature_1",$outPutArray)) ? $outPutArray['app_screen_colors_feature_1'] :  '' !!}</li>
                                <li>{!! ($outPutArray && array_key_exists("app_screen_colors_feature_2",$outPutArray)) ? $outPutArray['app_screen_colors_feature_2'] :  '' !!}</li>
                            </ul>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fpanel three">
                <div class="row justify-content-center">
                    <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-3.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_wakeup_title",$outPutArray)) ? $outPutArray['app_screen_wakeup_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_wakeup_detail_1",$outPutArray)) ? $outPutArray['app_screen_wakeup_detail_1'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_wakeup_detail_2",$outPutArray)) ? $outPutArray['app_screen_wakeup_detail_2'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_wakeup_detail_3",$outPutArray)) ? $outPutArray['app_screen_wakeup_detail_3'] :  '' !!}</p>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fpanel four">
                <div class="row justify-content-center">
                    <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-4.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_fallasleep_title",$outPutArray)) ? $outPutArray['app_screen_fallasleep_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_fallasleep_detail_1",$outPutArray)) ? $outPutArray['app_screen_fallasleep_detail_1'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_fallasleep_detail_2",$outPutArray)) ? $outPutArray['app_screen_fallasleep_detail_2'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_fallasleep_detail_3",$outPutArray)) ? $outPutArray['app_screen_fallasleep_detail_3'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_fallasleep_detail_4",$outPutArray)) ? $outPutArray['app_screen_fallasleep_detail_4'] :  '' !!}</p>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fpanel five">
                <div class="row justify-content-center">
                <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-5.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_lighttherapy_title",$outPutArray)) ? $outPutArray['app_screen_lighttherapy_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_lighttherapy_detail_1",$outPutArray)) ? $outPutArray['app_screen_lighttherapy_detail_1'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_lighttherapy_detail_2",$outPutArray)) ? $outPutArray['app_screen_lighttherapy_detail_2'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_lighttherapy_detail_3",$outPutArray)) ? $outPutArray['app_screen_lighttherapy_detail_3'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_lighttherapy_detail_4",$outPutArray)) ? $outPutArray['app_screen_lighttherapy_detail_4'] :  '' !!}</p>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fpanel six">
                <div class="row justify-content-center">
                <div class="col-7 col-md-3 offset-md-4">
                        <img src="{{ asset('public/assets/frontend/img/feature-6.png') }}" alt="" class="mb-3 mb-md-0 img-fluid">
                    </div>
                    <div class="col-11 col-md-5 d-flex flex-column justify-content-center text-center text-md-start fpaneldescheight">
                        <h2 class="h3 fw-bold">{!! ($outPutArray && array_key_exists("app_screen_analysis_title",$outPutArray)) ? $outPutArray['app_screen_analysis_title'] :  '' !!}</h2>
                        <div class="showmorehidecontent hideContent">
                            <p>{!! ($outPutArray && array_key_exists("app_screen_analysis_detail_1",$outPutArray)) ? $outPutArray['app_screen_analysis_detail_1'] :  '' !!}</p>
                            <p>{!! ($outPutArray && array_key_exists("app_screen_analysis_detail_2",$outPutArray)) ? $outPutArray['app_screen_analysis_detail_2'] :  '' !!}</p>
                        </div>
                        <div class="showmorebtn">
                            <span>Show more</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@section('script')
@endsection