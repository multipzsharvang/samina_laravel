<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css ') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}">EN</option>
                    <option value="{{ route('user.manualDe') }}" selected>DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Einführung</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
							<a href="{{ route('de.introduction') }}" class="nav-link smooth-scroll">Einführung</a>
							<a href="{{ route('de.generaldescription') }}" class="nav-link smooth-scroll">Allgemeine Beschreibung</a>
							<a href="{{ route('de.intendeduse') }}" class="nav-link smooth-scroll">Vorgesehener Verwendungszweck</a>
                            <a href="{{ route('de.colorlighttherapy') }}" class="nav-link smooth-scroll">Die Wirkung der Farb-Lichttherapie</a>
							<a href="{{ route('de.safetyimportant') }}" class="nav-link smooth-scroll">Wichtige Sicherheitshinweise</a>
							<a href="{{ route('de.electromagnetic') }}" class="nav-link smooth-scroll">Elektromagnetische Felder (EMF)</a>
                            <a href="{{ route('de.updatefirmware') }}" class="nav-link smooth-scroll">Firmware-Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="true">System und Funktionen</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="systemfunction-collapse">
							<a href="{{ route('de.setupdevice') }}" class="nav-link smooth-scroll">Das Gerät einrichten</a>
							<a href="{{ route('de.appledevices') }}" class="nav-link smooth-scroll">Apple Smartphones - iOS spezifische Einrichtung</a>
							<a href="{{ route('de.readingmode') }}" class="nav-link smooth-scroll">Modus Lesen</a>
                            <a href="{{ route('de.wakeuphelp') }}" class="nav-link smooth-scroll">Weckfunktionen</a>
                            <a href="{{ route('de.sleepaid') }}" class="nav-link smooth-scroll">Einschlafhilfe</a>
							<a href="{{ route('de.lightcontrol') }}" class="nav-link smooth-scroll">Licht-Steuerung</a>
							<a href="{{ route('de.musicmeditations') }}" class="nav-link smooth-scroll active">Musik-Medizin, Programme und Meditationen</a>
                            <a href="{{ route('de.lightcolortherapy') }}" class="nav-link smooth-scroll">Farb-Lichttherapie</a>
                            <a href="{{ route('de.personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('de.offlineuse') }}" class="nav-link smooth-scroll">Offline-Verwendung</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Fehlerbehebung</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Aufbewahrung und Pflege</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Garantie und Support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technische Daten</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="musicmeditations" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Musik-Medizin, Programme und Meditationen</h4>
                        <p class="mb-5">
                            <img src="{{ asset('public/assets/frontend/manual/img/german/light-control-8.png') }}"
                                alt="" class="img-fluid float-end ms-3">
                            Sowohl die Musik-Medizin (Musical Healing), die FarbLichttherapie sowie der
                            Einfluss von Meditationen,
                            Atemübungen, Suggestionen oder Hypnose sind in der Zwischenzeit in ihren
                            positiven Wirkungen wissenschaftlich gut fundiert. Das SAMINA Sound Light
                            Sleepsystem vereint erstmalig diese Therapiemöglichkeiten in einem einzigen
                            Gerät. Durch die parallele Anwendung entstehen bisher nicht gekannte
                            positive Synergien zwischen verschiedenen Heilsystemen. Ausgewählte
                            Musik-Kompositionen besitzen ein großes stressreduzierendes,
                            schlafförderndes und heilsames Potenzial. Dasselbe trifft auf die
                            Farb-Lichttherapie zu. Bereits die Kombination von Musik-Medizin und
                            Farb-Lichttherapie führen zu einer Kohärenz (Gleichklang) zwischen Körper
                            und Geist. Dadurch wird vor allem die Schlaf- und Regenerationsqualität
                            verbessert und in der Folge werden die Selbstheilungs-, Selbstregulations-
                            und Regenerationskräfte in uns aktiviert.
                        </p>
                        <div class="mb-3 h5 fw-bold">Programme / Meditationen / Musik</div>
                        <p>Die Programme sind jeweils in zwei Zonen aufgeteilt. In der oberen Sektion
                            wird das Programm
                            ausgewählt und der Inhalt abgespielt. In der unteren Sektion gelangen Sie
                            zur Konfiguration der
                            Programme.</p>
                        <div class="text-center">
                            <img src="{{ asset('public/assets/frontend/manual/img/german/light-control-9.png') }}"
                                alt="" class="img-fluid mb-3">
                        </div>
                        <div class="mb-3 h5 fw-bold">Programme / Wiedergabelisten erstellen</div>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/german/playlist-1.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8">
                                <p>Tippen Sie auf <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/add.png') }}"
                                            alt="" class="img-fluid"></span> um die jeweiligen Inhalte
                                    der ausgewählten Kategorie anzuzeigen.</p>
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <img src="{{ asset('public/assets/frontend/manual/img/german/playlist-2.png') }}"
                                            alt="" class="img-fluid mb-5">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3 h5 fw-bold">Inhalte</div>
                                        <p>Selektieren Sie den gewünschten Inhalt (Programme,
                                            Meditationen, Musiktitel).</p>
                                        <p>Tippen Sie auf <span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/transfer.png') }}"
                                                    alt="" class="img-fluid"></span> um den gewünschten
                                            Inhalt ins Hauptmenü zu übertragen.</p>
                                        <div class="mb-3 h5 fw-bold">Premium Inhalt</div>
                                        <p><span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/euro.png') }}"
                                                    alt="" class="img-fluid"></span> Premiuminhalte
                                            weisen ein
                                            € Symbol auf. Diese können Sie zum jeweiligen Preis
                                            kostenpflichtig erwerben.</p>
                                    </div>
                                </div>

                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <img src="{{ asset('public/assets/frontend/manual/img/german/playlist-3.png') }}"
                                        alt="" class="img-fluid">
                                </div>
                                <div class="col-md-7">
                                    <div class="mb-3 h5 fw-bold">Programm starten</div>
                                    <p>Bestimmen Sie das gewünschte Programm und tippen Sie einfach
                                        einmal in die <span class="text-primary fw-bold">«OBERE
                                            SEKTION»</span> um das Programm zu starten.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>©
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script>
        /* $(window).on('load', function() {
            $('#appDownload').modal('show');
        }); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
    </script>

</body>

</html>