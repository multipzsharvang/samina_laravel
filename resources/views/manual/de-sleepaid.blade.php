<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css ') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}">EN</option>
                    <option value="{{ route('user.manualDe') }}" selected>DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Einführung</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
							<a href="{{ route('de.introduction') }}" class="nav-link smooth-scroll">Einführung</a>
							<a href="{{ route('de.generaldescription') }}" class="nav-link smooth-scroll">Allgemeine Beschreibung</a>
							<a href="{{ route('de.intendeduse') }}" class="nav-link smooth-scroll">Vorgesehener Verwendungszweck</a>
                            <a href="{{ route('de.colorlighttherapy') }}" class="nav-link smooth-scroll">Die Wirkung der Farb-Lichttherapie</a>
							<a href="{{ route('de.safetyimportant') }}" class="nav-link smooth-scroll">Wichtige Sicherheitshinweise</a>
							<a href="{{ route('de.electromagnetic') }}" class="nav-link smooth-scroll">Elektromagnetische Felder (EMF)</a>
                            <a href="{{ route('de.updatefirmware') }}" class="nav-link smooth-scroll">Firmware-Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="true">System und Funktionen</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="systemfunction-collapse">
							<a href="{{ route('de.setupdevice') }}" class="nav-link smooth-scroll">Das Gerät einrichten</a>
							<a href="{{ route('de.appledevices') }}" class="nav-link smooth-scroll">Apple Smartphones - iOS spezifische Einrichtung</a>
							<a href="{{ route('de.readingmode') }}" class="nav-link smooth-scroll">Modus Lesen</a>
                            <a href="{{ route('de.wakeuphelp') }}" class="nav-link smooth-scroll">Weckfunktionen</a>
                            <a href="{{ route('de.sleepaid') }}" class="nav-link smooth-scroll active">Einschlafhilfe</a>
							<a href="{{ route('de.lightcontrol') }}" class="nav-link smooth-scroll">Licht-Steuerung</a>
							<a href="{{ route('de.musicmeditations') }}" class="nav-link smooth-scroll">Musik-Medizin, Programme und Meditationen</a>
                            <a href="{{ route('de.lightcolortherapy') }}" class="nav-link smooth-scroll">Farb-Lichttherapie</a>
                            <a href="{{ route('de.personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('de.offlineuse') }}" class="nav-link smooth-scroll">Offline-Verwendung</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Fehlerbehebung</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Aufbewahrung und Pflege</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Garantie und Support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technische Daten</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="sleepaid" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Einschlafhilfe</h4>
                        <p>Der Einschlaf-Modus des SAMINA Sound Light Sleepsystem dimmt über einen von
                            Ihnen
                            festgelegten Zeitraum hinweg ein schlafförderndes Licht mit einem hohem
                            RotlichtAnteil
                            (Sonnenuntergang). Dies ohne schlafstörenden Blaulicht-Anteil und komplett
                            flackerfrei.
                            Dadurch wird die Produktion von Melatonin in der Zirbeldrüse unterstützt,
                            Körper und Geist
                            werden auf den Schlaf vorbereitet. So gleiten Sie sanft in einen erholsamen,
                            bioenergetischen
                            Schlaf®. Dies kann durch schlaffördernde Musik-Kompositionen oder durch eine
                            Einschlafmeditation ergänzt werden. </p>
                        <div class="mb-3 h5 fw-bold">Schlafhilfe einrichten</div>
                        <p>Klicken Sie im Hauptmenü auf das «Jetzt einschlafen» Symbol.</p>
                        <div class="col-md-12 text-center">
                            <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-1.png') }}"
                                alt="" class="img-fluid">
                        </div>
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Schritt I</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-2.png') }}"
                                    alt="" class="img-fluid">
                                <p>Neue Schlaf-Kombo hinzufügen</p>
                            </div>
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Schritt II</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-3.png') }}"
                                    alt="" class="img-fluid">
                                <p>Musik und die gewünschte Einschlafdauer bestimmen</p>
                            </div>
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Schritt III</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/set-volume.png') }}"
                                    alt="" class="img-fluid">
                                <p>Lautstärke zum Einschlafen einstellen</p>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Schritt IV</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-4.png') }}"
                                    alt="" class="img-fluid">
                                <p>Schlaffördernde Lichtunterstützung «EIN» oder «AUS»</p>
                            </div>
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Schritt V</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-5.png') }}"
                                    alt="" class="img-fluid mb-3">
                                <p>Wählen Sie das gewünschte Licht und speichern Sie Ihre Kombination
                                </p>
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-6.png') }}"
                                    alt="">
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-7.png') }}"
                                    alt="" class="img-fluid mb-3">
                            </div>
                            <div class="col-md-8">
                                <p>Sie können zu jeder Zeit mehrere verschiedene Kombinationen
                                    <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/add.png') }}"
                                            alt=""></span> erstellen, <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/edit.png') }}"
                                            alt=""></span> bearbeiten, <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/delete.png') }}"
                                            alt=""></span> speichern oder löschen. Variieren Sie
                                    zwischen verschiedener Musik, verschiedenen
                                    Einschlaflichtern und verschiedenen Zeitspannen.
                                </p>
                            </div>
                        </div>
                        <div class="mb-3 h5 fw-bold">Hinweis</div>
                        <p><img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-8.png') }}"
                                alt="" class="float-start me-3 img-fluid"> Die Lichtintensität und die
                            Musiklautstärke orientieren sich an
                            der eingestellten «Einschlafdauer». Nach dem Starten der
                            Einschlafhilfe erklingt die Musik und das Licht in einer
                            gesteigerten Intensität. Diese nimmt über die von
                            Ihnen eingestellte «Einschlafdauer» allmählich ab. Sobald die eingestellte
                            Zeit
                            abgelaufen ist, schaltet das Licht, die Musik als auch die App des
                            Mobiltelefons ab.
                            Sie müssen sich somit um nichts mehr kümmern.</p>
                        <div class="mb-3 h5 fw-bold">Einschlafhilfe ins Hauptmenü einbinden</div>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-9.png') }}"
                                    alt="" class="img-fluid mb-3">
                            </div>
                            <div class="col-md-8">
                                <p>Nachdem Sie Ihre Einschlafkombinationen erfolgreich
                                    erstellt haben, wählen Sie eine bevorzugte Kombination
                                    aus und kehren anschliessend zurück ins Hauptmenü.
                                </p>
                                <div class="mb-3 h5 fw-bold">Einschlafhilfe starten</div>
                                <p>Sobald Sie die gewünschte Einschlafhilfe selektiert haben, wird diese
                                    neu im Hauptmenü erscheinen.</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-10.png') }}"
                                            alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-8">
                                        <p>Schalten Sie die Schaltfläche auf «EIN», <span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/switch-toggle.png') }}"
                                                    alt=""></span> um die Schlafhilfe zu starten. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-11.png') }}"
                                                    alt="" class="img-fluid">
                                            </div>
                                            <div class="col-md-6">
                                                <p>Nach dem Start beginnt der Countdown. </p>
                                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-12.png') }}"
                                                    alt="" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 h5 fw-bold">Einschlafhilfe wechseln, bearbeiten oder neu
                            kreieren </div>
                        <div class="row align-items-center">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/german/sleep-aid-13.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8">
                                <p>Wenn Sie eine andere Einschlafhilfe auswählen, die aktuell
                                    eingestellte Einschlafhilfe bearbeiten oder eine neue erstellen
                                    möchten, tippen Sie im Menü auf «Schlaf-Combo ändern». <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/german/sleep-combo.png') }}"
                                            alt="" class="img-fluid"></span> Dadurch gelangen Sie wieder
                                    zurück zum Einschlaf-Menü. </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>©
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script>
        /* $(window).on('load', function() {
            $('#appDownload').modal('show');
        }); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
    </script>

</body>

</html>