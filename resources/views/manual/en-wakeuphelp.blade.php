<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="true">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll active">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="wakeuphelp" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Wake up help</h4>
                        <p>The Sound Light Sleep system has two different wake-up methods.<br /> The
                            nature alarm clock simulates, as the name already suggests, the
                            nature-related waking up. the light therapy lamp starts thereby with little
                            light and quiet tones, these become gradually brighter with the adjusted
                            waking up time span and the volume rises gradually. As a result, you will
                            not be abruptly torn from sleep and start the new day with full vigor.</p>
                        <div class="mb-3 h5 fw-bold">Locate alarm clock</div>
                        <p>There are two different ways to locate the wake-up aid.</p>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <p>a) In the home screen</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/locate-alarm-1.png') }}"
                                    alt="" class="img-fluid" loading="lazy">
                            </div>
                            <div class="col-md-6 mb-3">
                                <p>b) In the alarm main menu</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/locate-alarm-2.png') }}"
                                    alt="" class="img-fluid" loading="lazy">
                            </div>
                        </div>
                        <div class="mb-3 h5 fw-bold">Set alarm clock</div>
                        <p>To enter the alarm configuration, tap the icon. <span
                                class="text-primary fs-4">+</span></p>
                        <img src="{{ asset('public/assets/frontend/manual/img/set-alarm.png') }}" alt=""
                            class="img-fluid" loading="lazy">

                        <div class="row mt-4">
                            <div class="col-md-6 text-center">
                                <div class="mb-1 h5 fw-bold">Natural wake up</div>
                                <p class="text-primary h5">Step I</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-1.png') }}"
                                    alt="" class="img-fluid">
                                <p>Set time</p>

                                <p class="text-primary h5">Step II</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-2.png') }}"
                                    alt="" class="img-fluid">
                                <p>Determine weekday/s</p>

                                <p class="text-primary h5">Step III</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-3.png') }}"
                                    alt="" class="img-fluid">
                                <p>Select alarm type "natural wake up"</p>

                                <p class="text-primary h5">Step IV</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/alarm-music.png') }}"
                                    alt="" class="img-fluid">
                                <p>Determine wake up music Mode ON/OFF and Select Music</p>

                                <p class="text-primary h5">Step V</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-5.png') }}"
                                    alt="" class="img-fluid">
                                <p>Determine wake up time Sunrise Mode On / Off</p>

                                <p class="text-primary h5">Step VI</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-6.png') }}"
                                    alt="" class="img-fluid">
                                <p>Determine day phase at the time of getting up</p>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="mb-1 h5 fw-bold">Standard Alarm</div>
                                <p class="text-primary h5">Step I</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-1.png') }}"
                                    alt="" class="img-fluid">
                                <p>Set time</p>

                                <p class="text-primary h5">Step II</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-2.png') }}"
                                    alt="" class="img-fluid">
                                <p>Determine weekday/s</p>

                                <p class="text-primary h5">Step III</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/wake-up-3-standard.png') }}"
                                    alt="" class="img-fluid">
                                <p>Select alarm type "Standard Alarm"</p>

                                <!-- <p class="text-primary h5">Step IV</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/alarm-music-default.png') }}" alt="" class="img-fluid">
                                <p>Select Music</p> -->
                            </div>
                        </div>

                        <div class="mb-3 h5 fw-bold">Alarm clock features</div>
                        <p>After the alarm clock is set, it will be displayed in the main menu under the
                            category "Sleep and wake-up help" of the app. Here, you can edit, turn on or
                            off the wake-up function at any time. </p>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ asset('public/assets/frontend/manual/img/clock-feature-1.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3 h5 fw-bold">Note</div>
                                <p>At homescreen the upcoming alarms are displayed, if the alarm process
                                    is already completed, the alarm will not appear again until 00:00 on
                                    the following day. </p>
                                <img src="{{ asset('public/assets/frontend/manual/img/clock-feature-2.png') }}"
                                    alt="" class="img-fluid mb-3">
                                <p>If the wake-up function is not displayed in the main menu, you can
                                    find your alarms in the alarm main menu. </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <img src="{{ asset('public/assets/frontend/manual/img/wake-up-screen.png') }}"
                                            alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3 h5 fw-bold">Wake-up screen</div>
                                        <p>As soon as the wake-up process starts, you will receive a
                                            notification by cell phone and the wake-up screen will
                                            appear. </p>
                                        <p>The system will start the wake-up process: the light and
                                            music will be gradually turned up and you will be gently
                                            woken up from your sleep.</p>
                                        <p>You can delay the alarm again via "Snooze" or end the alarm
                                            process prematurely if desired. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>