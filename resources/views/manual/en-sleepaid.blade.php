<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="true">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll active">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="sleepaid" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Sleep aid</h4>
                        <p>The sleep mode of the SAMINA Sound Light Sleepsystem dims a sleep-supporting
                            light with a high red light component (sunset) over a period of time
                            specified by you, this without a sleep disturbing blue light component and
                            completely flicker-free. This supports the production of melatonin in the
                            pineal gland, preparing body and mind for sleep. Thus, you glide gently into
                            a restful, Bioenergetic Sleep®. This can be complemented by sleep-promoting
                            music compositions or by a meditation to fall asleep.</p>
                        <div class="mb-3 h5 fw-bold">Sleep Aid Set Up </div>
                        <p>Click the "Go to sleep now" icon in the main menu.</p>
                        <div class="col-md-12 text-center">
                            <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-1.png') }}"
                                alt="" class="img-fluid">
                        </div>
                        <div class="row text-center">
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Step I</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-2.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Step II</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-3.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-6 col-lg-4 mb-3">
                                <p class="text-primary h5">Step III</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/set-volume.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <p class="mb-3">Add new sleep combo music and set the desired time and also
                                set Volume to fall
                                asleep.</p>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-6 mb-3">
                                <p class="text-primary h5">Step IV</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-4.png') }}"
                                    alt="" class="img-fluid">
                                <p>Sleep promoting light support "ON" or "OFF</p>
                            </div>
                            <div class="col-md-6 mb-3">
                                <p class="text-primary h5">Step V</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-5.png') }}"
                                    alt="" class="img-fluid mb-3">
                                <p>Select the desired light and save your combination</p>
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-6.png') }}"
                                    alt="">
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-7.png') }}"
                                    alt="" class="img-fluid mb-3">
                            </div>
                            <div class="col-md-8">
                                <p>You can create, <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/edit.png') }}"
                                            alt=""></span> edit,
                                    save or <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/delete.png') }}"
                                            alt=""></span> delete
                                    several different combinations <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/add.png') }}"
                                            alt=""></span> at any time. Vary between different music,
                                    different sleep-in lights and different time periods. </p>
                            </div>
                        </div>
                        <div class="mb-3 h5 fw-bold">Info</div>
                        <p><img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-8.png') }}"
                                alt="" class="float-start me-3 img-fluid">
                            The light intensity and the music volume are based on the set "sleep
                            duration". After starting the sleep aid, the music and light will increase
                            intensity, which gradually decreases over the "sleep duration" set by you.
                            As soon as the set time has elapsed, the light, the music as well as the app
                            of the cell phone switches off. You therefore no longer have to worry about
                            anything. </p>
                        <div class="mb-3 h5 fw-bold">Include sleep aid in the main menu</div>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-9.png') }}"
                                    alt="" class="img-fluid mb-3">
                            </div>
                            <div class="col-md-8">
                                <p>After you have successfully created your sleep combinations, select a
                                    favorite combination and then return to the main menu.</p>
                                <div class="mb-3 h5 fw-bold">Start sleep aid</div>
                                <p>Once you have selected the desired sleep aid, it will reappear in the
                                    main menu.</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-10.png') }}"
                                            alt="" class="img-fluid">
                                    </div>
                                    <div class="col-md-8">
                                        <p>Switch the button to "ON" <span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/switch-toggle.png') }}"
                                                    alt=""></span> to
                                            start the sleep aid. </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-11.png') }}"
                                                    alt="" class="img-fluid">
                                            </div>
                                            <div class="col-md-6">
                                                <p>After the start, the countdown begins. </p>
                                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-12.png') }}"
                                                    alt="" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 h5 fw-bold">Change, edit or create new sleep aid </div>
                        <div class="row align-items-center">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/sleep-aid-13.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8">
                                <p>If you want to select a different sleep aid, edit the currently set
                                    sleep aid or create a new one, tap "Change sleep combo" <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/sleep-combo.png') }}"
                                            alt="" class="img-fluid"></span>
                                    in the menu. This will take you back to the Sleep menu. </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>