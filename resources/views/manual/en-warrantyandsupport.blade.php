<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="flase">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="false">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0 active">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="warrantyandsupport" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Warranty and support</h4>
                        <div class="mb-3 h5 fw-bold">Warranty</div>
                        <p>The SAMINA Sound Light Sleep System includes a two-year warranty on all
                            electronic parts.</p>
                        <p>Note: Damage caused by external influences, such as damage caused by falling,
                            moisture damage, etc., is not covered.</p>
                        <div class="mb-3 h5 fw-bold">Technical Support</div>
                        <ul>
                            <li>Light therapy lamp</li>
                            <li>Apps iOS / Android</li>
                        </ul>
                        <p>If you need information or advice, visit <a href="http://www.samina.io"
                                class="text-primary">www.samina.io</a> or use our ticket support system
                            directly in the app. You can also contact the SAMINA Center in your country.
                        </p>
                        <div class="mb-3 h5 fw-bold">Ticket System App</div>
                        <div class="row mb-3 align-items-center">
                            <div class="col-auto">
                                <img src="{{ asset('public/assets/frontend/manual/img/open-side-menu.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col">
                                <p>Open the side menu in the app.</p>
                            </div>
                        </div>
                        <div class="row align-items-center mb-5">
                            <div class="col-auto">
                                <img src="{{ asset('public/assets/frontend/manual/img/side-menu.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col">
                                <p>Then open the category "Customer support" and describe your problem.
                                </p>
                            </div>
                        </div>
                        <div class="mb-2 h5 fw-bold">Support sound pillow</div>
                        <p class="text-primary fw-bold mb-0">SAMINA Production and Trading Ltd.</p>
                        <ul class="list-unstyled">
                            <li><img src="{{ asset('public/assets/frontend/manual/img/map-marker.png') }}"
                                    alt="" class="img-fluid me-1"><span class="align-middle">Obere Lände
                                    7 | 6820 Frastanz | Austria</span>
                            </li>
                            <li><img src="{{ asset('public/assets/frontend/manual/img/call.png') }}"
                                    alt="" class="img-fluid me-1"><span class="align-middle">T <a
                                        class="text-primary" href="tel:+43(0)552253500">+43 (0) 5522
                                        53500</a> | F +43 (0)
                                    5522 53500-20</span></li>
                            <li><img src="{{ asset('public/assets/frontend/manual/img/mail.png') }}"
                                    alt="" class="img-fluid me-1"><span class="align-middle"><a
                                        class="text-primary"
                                        href="mailto:samina@samina.com">samina@samina.com</a> </span>
                            </li>
                            <li><img src="{{ asset('public/assets/frontend/manual/img/website.png') }}"
                                    alt="" class="img-fluid me-1"><span class="align-middle"><a
                                        class="text-primary"
                                        href="http://www.samina.com">www.samina.com</a> </span></li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>