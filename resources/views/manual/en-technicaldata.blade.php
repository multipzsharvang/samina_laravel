<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="false">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0 active">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="technicaldata" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Technical data</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary align-middle">
                                <thead>
                                    <tr>
                                        <th colspan="2">Adapter</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Manufacturer</td>
                                        <td>Applie AG</td>
                                    </tr>
                                    <tr>
                                        <td>Type number</td>
                                        <td>1.10 Shenzhen</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal input voltage</td>
                                        <td>100 - 240 V AC</td>
                                    </tr>
                                    <tr>
                                        <td>Rated input frequency</td>
                                        <td>50 / 60 Hz</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal output voltage</td>
                                        <td>5.0 V DC</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal output current</td>
                                        <td>2.00 A</td>
                                    </tr>
                                    <tr>
                                        <td>Rated output voltage</td>
                                        <td>10.0 W</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal output current</td>
                                        <td>2.00 A</td>
                                    </tr>
                                    <tr>
                                        <td>Rated output power</td>
                                        <td>10 W</td>
                                    </tr>
                                    <tr>
                                        <td>Without load</td>
                                        <td>
                                            < 0.1 W</td>
                                    </tr>
                                    <tr>
                                        <td>Efficiency class</td>
                                        <td>VI</td>
                                    </tr>
                                    <tr>
                                        <td>Dimensions (height x width x depth)</td>
                                        <td>21 x 36 x 64 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Weight</td>
                                        <td>Approx. 0.12 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Cable length</td>
                                        <td>Approx. 150 cm</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Main unit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Manufacturer</td>
                                        <td>Applie AG</td>
                                    </tr>
                                    <tr>
                                        <td>Type number</td>
                                        <td>1.0 SAMINA Sound Light Sleepsystem</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal input voltage</td>
                                        <td>10 V DC</td>
                                    </tr>
                                    <tr>
                                        <td>Nominal input current</td>
                                        <td>1.75 A</td>
                                    </tr>
                                    <tr>
                                        <td>Input power</td>
                                        <td>15 W</td>
                                    </tr>
                                    <tr>
                                        <td>Safety classification</td>
                                        <td>Class III luminaire</td>
                                    </tr>
                                    <tr>
                                        <td>Dimensions (height x width x depth)</td>
                                        <td>Approx. 167 x 105 x 105 mm</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Connections</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>USB-C charging</td>
                                        <td>5 V DC, 2 A</td>
                                    </tr>
                                    <tr>
                                        <td>AUX sensitivity</td>
                                        <td>280 mV</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Light / LED</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Manufacturer</td>
                                        <td>Luminus Devices</td>
                                    </tr>
                                    <tr>
                                        <td>Product category</td>
                                        <td>High Power LED - Multicolor</td>
                                    </tr>
                                    <tr>
                                        <td>Product</td>
                                        <td>RGBW LED</td>
                                    </tr>
                                    <tr>
                                        <td>Mounting type</td>
                                        <td>SMD/SMT</td>
                                    </tr>
                                    <tr>
                                        <td>Illumination color</td>
                                        <td>RGB, White</td>
                                    </tr>
                                    <tr>
                                        <td>Wavelength/color temperature</td>
                                        <td>623 nm, 525 nm, 454 nm, 6500 K</td>
                                    </tr>
                                    <tr>
                                        <td>Luminous flux/radiant power</td>
                                        <td>90 lm, 210 lm, 210 lm, 1000 mW</td>
                                    </tr>
                                    <tr>
                                        <td>If - forward current</td>
                                        <td>1 A</td>
                                    </tr>
                                    <tr>
                                        <td>Vf - forward voltage</td>
                                        <td>3.6 V, 3.9 V, 3.9, 3.9 V</td>
                                    </tr>
                                    <tr>
                                        <td>Lens color</td>
                                        <td>Colorless</td>
                                    </tr>
                                    <tr>
                                        <td>Lens shape</td>
                                        <td>Rectangular</td>
                                    </tr>
                                    <tr>
                                        <td>Lens transparency</td>
                                        <td>Transparent</td>
                                    </tr>
                                    <tr>
                                        <td>Height</td>
                                        <td>1.21 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Length</td>
                                        <td>5.75 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum operating temperature</td>
                                        <td>+ 100 C</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>