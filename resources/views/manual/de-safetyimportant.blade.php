<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css ') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}">EN</option>
                    <option value="{{ route('user.manualDe') }}" selected>DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="true">Einführung</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="home-collapse">
							<a href="{{ route('de.introduction') }}" class="nav-link smooth-scroll">Einführung</a>
							<a href="{{ route('de.generaldescription') }}" class="nav-link smooth-scroll">Allgemeine Beschreibung</a>
							<a href="{{ route('de.intendeduse') }}" class="nav-link smooth-scroll">Vorgesehener Verwendungszweck</a>
                            <a href="{{ route('de.colorlighttherapy') }}" class="nav-link smooth-scroll">Die Wirkung der Farb-Lichttherapie</a>
							<a href="{{ route('de.safetyimportant') }}" class="nav-link smooth-scroll active">Wichtige Sicherheitshinweise</a>
							<a href="{{ route('de.electromagnetic') }}" class="nav-link smooth-scroll">Elektromagnetische Felder (EMF)</a>
                            <a href="{{ route('de.updatefirmware') }}" class="nav-link smooth-scroll">Firmware-Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="false">System und Funktionen</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="systemfunction-collapse">
							<a href="{{ route('de.setupdevice') }}" class="nav-link smooth-scroll">Das Gerät einrichten</a>
							<a href="{{ route('de.appledevices') }}" class="nav-link smooth-scroll">Apple Smartphones - iOS spezifische Einrichtung</a>
							<a href="{{ route('de.readingmode') }}" class="nav-link smooth-scroll">Modus Lesen</a>
                            <a href="{{ route('de.wakeuphelp') }}" class="nav-link smooth-scroll">Weckfunktionen</a>
                            <a href="{{ route('de.sleepaid') }}" class="nav-link smooth-scroll">Einschlafhilfe</a>
							<a href="{{ route('de.lightcontrol') }}" class="nav-link smooth-scroll">Licht-Steuerung</a>
							<a href="{{ route('de.musicmeditations') }}" class="nav-link smooth-scroll">Musik-Medizin, Programme und Meditationen</a>
                            <a href="{{ route('de.lightcolortherapy') }}" class="nav-link smooth-scroll">Farb-Lichttherapie</a>
                            <a href="{{ route('de.personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('de.offlineuse') }}" class="nav-link smooth-scroll">Offline-Verwendung</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Fehlerbehebung</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Aufbewahrung und Pflege</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Garantie und Support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technische Daten</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="safetyimportant" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Wichtige Sicherheitshinweise</h4>
                        <p class="text-danger">Lesen Sie diese wichtigen Informationen vor dem Gebrauch
                            des Geräts
                            aufmerksam durch und bewahren Sie sie für eine spätere Verwendung
                            auf.</p>
                        <h5 class="h5 fw-bold">Gefahr</h5>
                        <ul>
                            <li>Wasser und Elektrizität sind eine gefährliche Kombination! Benutzen Sie
                                dieses Gerät nicht in
                                feuchter Umgebung (z. B. im Badezimmer, in der Nähe einer Dusche oder
                                eines
                                Schwimmbeckens).</li>
                            <li>Halten Sie das Netzteil trocken.</li>
                            <li>Achten Sie darauf, dass kein Wasser in das Gerät gelangt oder darüber
                                verschüttet wird.</li>
                            <li>Tauchen Sie das Gerät niemals in Wasser. Spülen Sie es auch nicht unter
                                fließendem Wasser
                                ab.</li>
                            <li>Stellen Sie nichts in die Nähe des Geräts, das dazu führen könnte, dass
                                Wasser oder andere
                                Flüssigkeiten auf das Gerät tropfen oder spritzen.</li>
                            <li>Dieses Gerät ist nur für den Gebrauch in Innenräumen vorgesehen.</li>
                            <li>Der Adapter enthält einen Transformator, der unsichere 100 - 240 V AC
                                Netzspannung in
                                sichere 24 V DC Niederspannung umwandelt. Ersetzen Sie den Adapter
                                keinesfalls durch
                                einen anderen Stecker, da dies den Benutzer und das Gerät gefährden
                                kann.</li>
                        </ul>
                        <h5 class="h5 fw-bold">Warnhinweis</h5>
                        <ul>
                            <li>Verwenden Sie das Sound-Kissen auf keinen Fall für Audiowiedergaben
                                außerhalb der
                                «SAMINA Sound Light Sleep App» (YouTube, SoundCloud, iTunes, Spotify
                                oder ähnliches).</li>
                            <li>Sehen Sie niemals direkt in das Licht, respektive direkt in die LED.
                            </li>
                            <li>Verwenden Sie das Gerät keinesfalls, wenn das Lampengehäuse beschädigt
                                oder zerbrochen
                                ist oder fehlt.
                            </li>
                            <li>Wenn Sie sich zu irgendeinem Zeitpunkt während einer Anwendung unwohl
                                fühlen, führen Sie
                                die Anwendung nicht fort.</li>
                            <li>Benutzen Sie das Gerät nur mit dem mitgelieferten Adapter.</li>
                            <li>Wenn das Netzteil beschädigt ist, darf es nur durch ein
                                Originalersatzteil ausgetauscht werden,
                                um Gefahren zu vermeiden.</li>
                            <li>Dieses Gerät ist für Benutzer (einschl. Kinder) mit eingeschränkten
                                physischen, sensorischen
                                oder psychischen Fähigkeiten bzw. ohne jegliche Erfahrung oder Vorwissen
                                nur dann geeignet,
                                wenn eine angemessene Aufsicht oder ausführliche Anleitung zur Benutzung
                                des Geräts durch
                                eine verantwortliche Person sichergestellt ist.
                            </li>
                            <li>Benutzen Sie dieses Gerät nicht, um Ihre Schlafzeit zu verkürzen. Der
                                Zweck dieses Geräts
                                ist es, Ihnen das Aufwachen zu erleichtern. Es verringert jedoch nicht
                                Ihren Schlafbedarf.</li>
                        </ul>
                        <h5 class="h5 fw-bold">Achtung</h5>
                        <ul>
                            <li>Prüfen Sie, bevor Sie das Gerät in Betrieb nehmen, ob die
                                Spannungsangabe auf dem Gerät
                                mit der örtlichen Netzspannung übereinstimmt.</li>
                            <li>Achten Sie darauf, nicht auf das Netzkabel zu treten oder es zu knicken.
                                Dies gilt insbesondere
                                an Steckern, Steckdosen oder am Austritt der Lampe.</li>
                            <li>Um einen irreparablen Schaden des Adapters zu vermeiden, verwenden Sie
                                den Adapter nicht
                                in der Nähe einer Wandsteckdose, an die ein elektrischer Lufterfrischer
                                angeschlossen ist.</li>
                            <li>Lassen Sie das Gerät nicht fallen und setzen Sie es keinerlei schweren
                                Stößen aus.</li>
                            <li>Stellen Sie das Gerät auf eine stabile, ebene und rutschfeste
                                Oberfläche.</li>
                            <li>Beeinträchtigen Sie nicht die Kühlung des Geräts, indem Sie das Gerät
                                mit Gegenständen wie
                                Decken, Gardinen, Kleidung, Papieren usw. abdecken.</li>
                            <li>Verwenden Sie das Gerät nicht bei Zimmertemperaturen unter 5° C oder
                                über 35° C.
                            </li>
                            <li>Das Gerät hat einen Ein-/Ausschalter. Um das Gerät auszuschalten, ziehen
                                Sie das Netzteil
                                nicht aus der Steckdose. Der Adapter und die Steckdose müssen jederzeit
                                leicht erreichbar
                                sein.</li>
                            <li>Dieses Gerät ist nur für den häuslichen Gebrauch oder in ähnlichem
                                Umfeld, wie z. B. im Hotel,
                                geeignet.</li>
                            <li>Es dürfen keine offenen Feuerquellen wie brennende Kerzen auf dem Gerät
                                abgestellt werden.</li>
                            <li>Die LEDs in diesem System sind nicht austauschbar. Wenn sie das Ende
                                ihrer Lebensdauer
                                erreichen, muss das gesamte Sound Light System ersetzt werden. Bei
                                korrekter Anwendung
                                hält die LED bis zu 40'000 Stunden.</li>
                        </ul>
                        <h5 class="h5 fw-bold">Allgemeines</h5>
                        <ul>
                            <li>Wenn Sie oft zu früh oder mit Kopfschmerzen aufwachen, verringern Sie
                                die eingestellte
                                Lichtintensität und/oder die eingestellte Dauer der
                                Sonnenaufgangssimulation.</li>
                            <li>Wenn Sie oft durch den Weckton geweckt werden, erhöhen Sie die
                                eingestellte Lichtintensität
                                und/oder die eingestellte Dauer der Sonnenaufgangssimulation.
                            </li>
                            <li>Wenn eine weitere Person im Zimmer schläft, kann diese Person
                                unbeabsichtigt durch das
                                Aufwach-Licht geweckt werden, obwohl sie weiter vom Gerät entfernt ist.
                                Das liegt daran, dass
                                Menschen unterschiedlich stark auf Licht reagieren.</li>
                            <li>Der Stromverbrauch des Geräts ist am niedrigsten, wenn kein Gerät
                                gekoppelt ist und die
                                Lampe, die Musik und das Licht ausgeschaltet sind.
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>©
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script>
        /* $(window).on('load', function() {
            $('#appDownload').modal('show');
        }); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
    </script>

</body>

</html>