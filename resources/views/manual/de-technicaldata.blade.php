<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css ') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}">EN</option>
                    <option value="{{ route('user.manualDe') }}" selected>DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Einführung</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
							<a href="{{ route('de.introduction') }}" class="nav-link smooth-scroll">Einführung</a>
							<a href="{{ route('de.generaldescription') }}" class="nav-link smooth-scroll">Allgemeine Beschreibung</a>
							<a href="{{ route('de.intendeduse') }}" class="nav-link smooth-scroll">Vorgesehener Verwendungszweck</a>
                            <a href="{{ route('de.colorlighttherapy') }}" class="nav-link smooth-scroll">Die Wirkung der Farb-Lichttherapie</a>
							<a href="{{ route('de.safetyimportant') }}" class="nav-link smooth-scroll">Wichtige Sicherheitshinweise</a>
							<a href="{{ route('de.electromagnetic') }}" class="nav-link smooth-scroll">Elektromagnetische Felder (EMF)</a>
                            <a href="{{ route('de.updatefirmware') }}" class="nav-link smooth-scroll">Firmware-Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="false">System und Funktionen</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="systemfunction-collapse">
							<a href="{{ route('de.setupdevice') }}" class="nav-link smooth-scroll">Das Gerät einrichten</a>
							<a href="{{ route('de.appledevices') }}" class="nav-link smooth-scroll">Apple Smartphones - iOS spezifische Einrichtung</a>
							<a href="{{ route('de.readingmode') }}" class="nav-link smooth-scroll">Modus Lesen</a>
                            <a href="{{ route('de.wakeuphelp') }}" class="nav-link smooth-scroll">Weckfunktionen</a>
                            <a href="{{ route('de.sleepaid') }}" class="nav-link smooth-scroll">Einschlafhilfe</a>
							<a href="{{ route('de.lightcontrol') }}" class="nav-link smooth-scroll">Licht-Steuerung</a>
							<a href="{{ route('de.musicmeditations') }}" class="nav-link smooth-scroll">Musik-Medizin, Programme und Meditationen</a>
                            <a href="{{ route('de.lightcolortherapy') }}" class="nav-link smooth-scroll">Farb-Lichttherapie</a>
                            <a href="{{ route('de.personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('de.offlineuse') }}" class="nav-link smooth-scroll">Offline-Verwendung</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Fehlerbehebung</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Aufbewahrung und Pflege</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Garantie und Support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('de.technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0 active">Technische Daten</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="technicaldata" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Technische Daten</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary align-middle">
                                <thead>
                                    <tr>
                                        <th colspan="2">Adapter</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hersteller</td>
                                        <td>Applie AG</td>
                                    </tr>
                                    <tr>
                                        <td>Typennummer</td>
                                        <td>1.10 Shenzhen</td>
                                    </tr>
                                    <tr>
                                        <td>Nenneingangsspannung</td>
                                        <td>100 - 240 V AC</td>
                                    </tr>
                                    <tr>
                                        <td>Nenneingangsfrequenz</td>
                                        <td>50 / 60 Hz</td>
                                    </tr>
                                    <tr>
                                        <td>Nennausgangsspannung</td>
                                        <td>5.0 V DC</td>
                                    </tr>
                                    <tr>
                                        <td>Nominaler Ausgansstrom</td>
                                        <td>2.00 A</td>
                                    </tr>
                                    <tr>
                                        <td>Nennausgangsspannung</td>
                                        <td>10.0 W</td>
                                    </tr>
                                    <tr>
                                        <td>Nominaler Ausgangsstrom</td>
                                        <td>2.00 A</td>
                                    </tr>
                                    <tr>
                                        <td>Nennausgangsleistung</td>
                                        <td>10 W</td>
                                    </tr>
                                    <tr>
                                        <td>Ohne Last</td>
                                        <td>
                                            < 0.1 W</td>
                                    </tr>
                                    <tr>
                                        <td>Effizienzklasse</td>
                                        <td>VI</td>
                                    </tr>
                                    <tr>
                                        <td>Abmessungen (Höhe x Breite x Tiefe)</td>
                                        <td>21 x 36 x 64 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Gewicht</td>
                                        <td>Ca. 0.12 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Kabellänge</td>
                                        <td>Ca. 150 cm</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Hauptgerät</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hersteller</td>
                                        <td>Applie AG</td>
                                    </tr>
                                    <tr>
                                        <td>Typennummer</td>
                                        <td>1.0 SAMINA Sound Light Sleepsystem</td>
                                    </tr>
                                    <tr>
                                        <td>Nenneingangsspannung</td>
                                        <td>10 V DC</td>
                                    </tr>
                                    <tr>
                                        <td>Nominaler Eingangsstrom</td>
                                        <td>1.75 A</td>
                                    </tr>
                                    <tr>
                                        <td>Eingangsleistung</td>
                                        <td>15 W</td>
                                    </tr>
                                    <tr>
                                        <td>Sicherheitsklassifizierung</td>
                                        <td>Leuchte der Klasse III</td>
                                    </tr>
                                    <tr>
                                        <td>Abmessungen (Höhe x Breite x Tiefe)</td>
                                        <td>Ca. 167 x 105 x 105 mm</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Anschlüsse</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>USB-C Aufladung</td>
                                        <td>5 V DC, 2 A</td>
                                    </tr>
                                    <tr>
                                        <td>AUX-Empfindlichkeit</td>
                                        <td>280 mV</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="2">Licht / LED</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Hersteller</td>
                                        <td>Luminus Devices</td>
                                    </tr>
                                    <tr>
                                        <td>Produktkategorie</td>
                                        <td>Hochleistungs-LED - Mehrfarbig</td>
                                    </tr>
                                    <tr>
                                        <td>Produkt</td>
                                        <td>RGBW LED</td>
                                    </tr>
                                    <tr>
                                        <td>Montageart</td>
                                        <td>SMD/SMT</td>
                                    </tr>
                                    <tr>
                                        <td>Beleuchtungsfarbe</td>
                                        <td>RGB, Weiss</td>
                                    </tr>
                                    <tr>
                                        <td>Wellenlänge/Farbtemeraturt</td>
                                        <td>623 nm, 525 nm, 454 nm, 6500 K</td>
                                    </tr>
                                    <tr>
                                        <td>Lichtstrom/Strahlungsleistung</td>
                                        <td>90 lm, 210 lm, 210 lm, 1000 mW</td>
                                    </tr>
                                    <tr>
                                        <td>If - Durchlassstrom</td>
                                        <td>1 A</td>
                                    </tr>
                                    <tr>
                                        <td>Vf - Durchlassspannung</td>
                                        <td>3.6 V, 3.9 V, 3.9, 3.9 V</td>
                                    </tr>
                                    <tr>
                                        <td>Linsenfarbe</td>
                                        <td>Farblos</td>
                                    </tr>
                                    <tr>
                                        <td>Linsenform</td>
                                        <td>Rechteckig</td>
                                    </tr>
                                    <tr>
                                        <td>Objektiv Transparenz</td>
                                        <td>Durchsichtig</td>
                                    </tr>
                                    <tr>
                                        <td>Höhe</td>
                                        <td>1.21 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Länge</td>
                                        <td>5.75 mm</td>
                                    </tr>
                                    <tr>
                                        <td>Maximale Betriebstemperatur </td>
                                        <td>+ 100 C</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>©
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script>
        /* $(window).on('load', function() {
            $('#appDownload').modal('show');
        }); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
    </script>

</body>

</html>