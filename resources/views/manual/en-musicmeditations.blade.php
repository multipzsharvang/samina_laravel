<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="false">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="true">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll active">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="musicmeditations" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Music medicine, programs and meditations</h4>
                        <p class="mb-md-5">
                            <img src="{{ asset('public/assets/frontend/manual/img/light-control-8.png') }}"
                                alt="" class="img-fluid float-end ms-3">
                            The positive effects of music medicine (musical healing), colored light
                            therapy and the influence of meditation, bre-athing exercises, suggestions
                            or hypnosis are scien-tifically well established. For the first time, the
                            SAMINA Sound Light Sleepsystem combines these therapy options in a single
                            device. The parallel application creates previously unknown positive
                            synergies between different healing systems. Selected music compositions
                            have great stress reducing, sleep promoting and healing potential. The same
                            is true of colored light therapy. Already the combination of music medicine
                            and color light therapy lead to
                            a coherence therapy lead to a coherence (harmony) between body and mind.
                            This improves especially the quality of sleep and regeneration and as a
                            result the self-healing, self-regulating and self-repairing forces within us
                            are activated.
                        </p>
                        <div class="mb-3 h5 fw-bold">Programs / Meditations / Music</div>
                        <p>The programs are each divided into two zones, in the upper section, the
                            program is selected and the content is played. In the lower section you get
                            to the configuration of the programs. </p>
                        <div class="text-center">
                            <img src="{{ asset('public/assets/frontend/manual/img/light-control-9.png') }}"
                                alt="" class="img-fluid mb-3">
                        </div>
                        <div class="mb-3 h5 fw-bold">Programs / Create playlists</div>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset('public/assets/frontend/manual/img/playlist-1.png') }}"
                                    alt="" class="img-fluid">
                            </div>
                            <div class="col-md-8">
                                <p>Tap to display <span><img
                                            src="{{ asset('public/assets/frontend/manual/img/add.png') }}"
                                            alt="" class="img-fluid"></span> the respective contents of
                                    the
                                    selected category. </p>
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <img src="{{ asset('public/assets/frontend/manual/img/playlist-2.png') }}"
                                            alt="" class="img-fluid mb-5">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3 h5 fw-bold">Content</div>
                                        <p>Select the desired content (programs, meditations, music
                                            tracks). </p>
                                        <p>Tap to <span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/transfer.png') }}"
                                                    alt="" class="img-fluid"></span> transfer the
                                            desired
                                            content to the main menu</p>
                                        <div class="mb-3 h5 fw-bold">Premium content</div>
                                        <p><span><img
                                                    src="{{ asset('public/assets/frontend/manual/img/euro.png') }}"
                                                    alt="" class="img-fluid"></span> Premium content has
                                            a €
                                            symbol, these can be purchased at the respective price.</p>
                                    </div>
                                </div>

                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-5">
                                    <img src="{{ asset('public/assets/frontend/manual/img/playlist-3.png') }}"
                                        alt="" class="img-fluid">
                                </div>
                                <div class="col-md-7">
                                    <div class="mb-3 h5 fw-bold">Start program</div>
                                    <p>Determine the desired program and simply tap once in the <span
                                            class="text-primary fw-bold">"UPPER SECTION"</span> to start
                                        the program.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>