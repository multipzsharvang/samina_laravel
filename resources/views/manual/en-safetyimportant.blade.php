<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('public/assets/frontend/manual/img/favicon.ico') }}" type="image/x-icon">
    <title>Samina | User Manual</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/assets/frontend/manual/css/user-manual.css') }}">
</head>

<body>
    <header class="navbar bg-light navbar-light sticky-top flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="{{ route('index') }}">
            <img src="{{ asset('public/assets/frontend/manual/img/logo.png') }}" alt="" class="img-fluid">
        </a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <div class="nav-item text-nowrap">
                <select class="form-select" onchange="location = this.value;">
                    <option value="{{ route('user.manualEn') }}" selected>EN</option>
                    <option value="{{ route('user.manualDe') }}">DE</option>
                </select>
            </div>
        </div>
    </header>

    <main class="container-fluid">
        <div class="row">
            <aside id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
					<nav id="manual-navbar" class="collapse show navbar mx-3 manual-sidebar">
                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#home-collapse" aria-expanded="true">Introduction</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse show ml-1" id="home-collapse">
                            <a href="{{ route('introduction') }}" class="nav-link smooth-scroll">Introduction</a>
							<a href="{{ route('generaldescription') }}" class="nav-link smooth-scroll">General description</a>
							<a href="{{ route('intendeduse') }}" class="nav-link smooth-scroll">Intended use</a>
                            <a href="{{ route('colorlighttherapy') }}" class="nav-link smooth-scroll">The effect of color light therapy</a>
							<a href="{{ route('safetyimportant') }}" class="nav-link smooth-scroll active">Important safety information</a>
							<a href="{{ route('electromagnetic') }}" class="nav-link smooth-scroll">Electromagnetic fields (EMF)</a>
                            <a href="{{ route('updatefirmware') }}" class="nav-link smooth-scroll">Firmware Update</a>
						</nav>

                        <button class="btn btn-toggle w-100 rounded manual-btn" data-bs-toggle="collapse" data-bs-target="#systemfunction-collapse" aria-expanded="false">System and functions</button>
						<nav class="btn-toggle-nav list-unstyled pb-1 collapse ml-1" id="systemfunction-collapse">
                            <a href="{{ route('setupdevice') }}" class="nav-link smooth-scroll">Set up the device</a>
							<a href="{{ route('appledevices') }}" class="nav-link smooth-scroll">Apple devices - iOS specific setup</a>
							<a href="{{ route('readingmode') }}" class="nav-link smooth-scroll">Reading Mode</a>
                            <a href="{{ route('wakeuphelp') }}" class="nav-link smooth-scroll">Wake up help</a>
                            <a href="{{ route('sleepaid') }}" class="nav-link smooth-scroll">Sleep aid</a>
							<a href="{{ route('lightcontrol') }}" class="nav-link smooth-scroll">Light control</a>
							<a href="{{ route('musicmeditations') }}" class="nav-link smooth-scroll">Music medicine, programs and meditations</a>
                            <a href="{{ route('lightcolortherapy') }}" class="nav-link smooth-scroll">Color light therapy</a>
                            <a href="{{ route('personalsleephealthcoach') }}" class="nav-link smooth-scroll">Personal Sleep & Health Coach</a>
							<a href="{{ route('offlineuse') }}" class="nav-link smooth-scroll">Offline use</a>
						</nav>
                        
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('troubleshooting') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Troubleshooting</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('storagecare') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Storage and care</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('recycling') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Recycling</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('warrantyandsupport') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Warranty and support</a>
                        </nav>
                        <nav class="btn-toggle-nav w-100 list-unstyled pb-1">
                            <a href="{{ route('technicaldata') }}" class="manual-btn nav-link smooth-scroll btn align-items-center rounded mx-0">Technical data</a>
                        </nav>
					</nav>
                </div>
            </aside>

            <div class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-3">
                <div class="manual-detail">
                    <section id="safetyimportant" class="pt-1">
                        <h4 class="h4 mb-3 text-primary">Important safety information</h4>
                        <p class="text-danger">Read this important information carefully before using
                            the device and keep it for later use.</p>
                        <h5 class="h5 fw-bold">Danger</h5>
                        <ul>
                            <li>Water and electricity are a dangerous combination! Do not use this
                                device in a damp environment (e.g. in the bathroom, near a shower or a
                                swimming pool).</li>
                            <li>Keep the power supply dry.</li>
                            <li>Make sure that no water gets into the device or is spilled over it.</li>
                            <li>Never immerse the device in water. Do not rinse it under running water
                                either.</li>
                            <li>Do not place anything near the device that could cause water or other
                                liquids to drip or splash onto the device.</li>
                            <li>This device is intended for indoor use only.</li>
                            <li>The adapter contains a transformer that converts unsafe 100-240 V AC
                                mains voltage into safe 24 V DC low voltage. Do not replace the adapter
                                with any other plug under any circumstances, as this may endanger the
                                user and the device.</li>
                        </ul>
                        <h5 class="h5 fw-bold">Warning</h5>
                        <ul>
                            <li>Never use the sound pillow for audio playback outside the "SAMINA Sound
                                Light Sleep App" (YouTube, SoundCloud, iTunes, Spotify or similar). The
                                speakers are exclusively designed for the special frequencies, volume
                                and tones of the Sound Light Sleep app.</li>
                            <li>Never look directly into the light or directly into the LED.</li>
                            <li>Never use the device if the lamp housing is damaged, broken or missing.
                                If the adapter, power cord, or fixture are damaged, liquid has been
                                spilled on or objects have fallen into and/or onto the fixture, the
                                fixture has been exposed to rain or moisture, or is not operating
                                properly or has been dropped or damaged, do not operate the fixture.
                            </li>
                            <li>If you feel uncomfortable at any time during an application, do not
                                continue the application.</li>
                            <li>Use the device only with the supplied adapter.</li>
                            <li>If the power supply unit is damaged, it must only be replaced with an
                                original spare part to avoid danger</li>
                            <li>This device is not intended for use by persons (including children) with
                                reduced physical, sensory or mental capabilities, or lack of experience
                                and knowledge, unless they have been given supervision or instruction
                                concerning use of the device by a person responsible for their safety.
                            </li>
                            <li>Do not use this device to shorten your sleep time. The purpose of this
                                device is to help you wake up. However, it does not reduce your need to
                                sleep.</li>
                        </ul>
                        <h5 class="h5 fw-bold">Attention</h5>
                        <ul>
                            <li>Before operating the device, check whether the voltage specification on
                                the device corresponds to the local mains voltage.</li>
                            <li>Take care not to step on or bend the power cord. This applies in
                                particular to plugs, sockets or the outlet from the lamp.</li>
                            <li>To avoid irreparable damage to the adapter, do not use the adapter in or
                                near a wall outlet to which an electric air freshener is connected.</li>
                            <li>Do not drop the device or subject it to heavy impact.</li>
                            <li>Place the device on a stable, level and non-slip surface.</li>
                            <li>Do not interfere with the cooling of the unit by covering the unit with
                                items such as blankets, curtains, clothing, papers, etc.</li>
                            <li>Do not use the device at room temperatures below 5℃ or above 35℃.
                            </li>
                            <li>The device has an on/off switch, to turn the device off, do not unplug
                                the power adapter from the wall outlet. The adapter and the power outlet
                                must be easily accessible at all times.</li>
                            <li>This device is only suitable for domestic use or in a similar
                                environment, such as a hotel.</li>
                            <li>No open fire sources such as lighted candles should be placed on the
                                unit.</li>
                            <li>The LEDs in this system are not replaceable. When they reach the end of
                                their life, the entire Sound Light System must be replaced. When used
                                correctly, the LED will last up to 40'000 hours.</li>
                        </ul>
                        <h5 class="h5 fw-bold">General</h5>
                        <ul>
                            <li>If you often wake up too early or with a headache, reduce the set light
                                intensity and/or the set duration of the sunrise simulation.</li>
                            <li>If you are often awakened by the alarm sound, increase the set light
                                intensity and/or the set duration of the sunrise simulation.</li>
                            <li>If another person is sleeping in the room, this person may be
                                unintentionally awakened by the wake-up light, even though he or she is
                                further away from the device. This is because people react differently
                                to light.</li>
                            <li>The power consumption of the device is lowest when no device is paired
                                via the USB port and the lamp, music and light are turned off.</li>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </main>

    <!-- <footer class="py-5 container-fluid">
		<div class="d-flex justify-content-center py-4 my-4 border-top">
			<p>�
				<script>document.write(new Date().getFullYear())</script> Samina. All rights reserved.
			</p>
		</div>
	</footer> -->

    <div id="appDownload" class="modal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">SAMINA | Sound Light Sleepsystem</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body bg-primary">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/app-download.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                        <div class="col-6">
                            <img src="{{ asset('public/assets/frontend/manual/img/register-device.png') }}" alt=""
                                class="img-fluid d-block mx-auto">
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div> -->
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
		/* $(window).on('load', function() {
			$('#appDownload').modal('show');
		}); */
        $('.nav-link').on('click', function(){
            if($(this).parent().hasClass('collapse') == false){
                $(this).parent().siblings('button').attr("aria-expanded","false");
            }
        });
		       
        $('.manual-btn').on('click', function(){
            if($('.btn-toggle-nav.collapse').length > 0){
                $('.btn-toggle-nav.collapse').removeClass('show');
                $(this).siblings('button').attr("aria-expanded","false");
                $('.nav-link').removeClass('active');
            }
        });
	</script>
</body>

</html>