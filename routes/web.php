<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which    
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Auth::routes(['register' => false]);
Route::get('clear', function () {
    \Artisan::call('config:cache');
    \Artisan::call('cache:clear');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    \Artisan::call('storage:link');
    echo "ok";
});
Route::get('/register', function () {
    return redirect('login');
});
Route::get('/', [App\Http\Controllers\WebsiteController::class, 'index'])->name('frontpage');
Route::get('/index', [App\Http\Controllers\WebsiteController::class, 'index'])->name('index');

Route::get('/privacy-policy', [App\Http\Controllers\WebsiteController::class, 'contentPage'])->name('privacy.policy');
Route::get('/terms-of-service', [App\Http\Controllers\WebsiteController::class, 'contentPage'])->name('terms.of.service');
Route::get('/cookies-policy', [App\Http\Controllers\WebsiteController::class, 'contentPage'])->name('cookies.policy');
Route::get('/returns-policy', [App\Http\Controllers\WebsiteController::class, 'contentPage'])->name('returns.policy');
Route::get('/shipping-terms', [App\Http\Controllers\WebsiteController::class, 'contentPage'])->name('shipping.terms');

Route::get('/en/user/manual', [App\Http\Controllers\WebsiteController::class, 'userManualEn'])->name('user.manualEn');
Route::get('/en/user/manual/introduction', [App\Http\Controllers\WebsiteController::class, 'introduction'])->name('introduction');
Route::get('/en/user/manual/generaldescription', [App\Http\Controllers\WebsiteController::class, 'generaldescription'])->name('generaldescription');
Route::get('/en/user/manual/intendeduse', [App\Http\Controllers\WebsiteController::class, 'intendeduse'])->name('intendeduse');
Route::get('/en/user/manual/colorlighttherapy', [App\Http\Controllers\WebsiteController::class, 'colorlighttherapy'])->name('colorlighttherapy');
Route::get('/en/user/manual/safetyimportant', [App\Http\Controllers\WebsiteController::class, 'safetyimportant'])->name('safetyimportant');
Route::get('/en/user/manual/electromagnetic', [App\Http\Controllers\WebsiteController::class, 'electromagnetic'])->name('electromagnetic');
Route::get('/en/user/manual/setupdevice', [App\Http\Controllers\WebsiteController::class, 'setupdevice'])->name('setupdevice');
Route::get('/en/user/manual/appledevices', [App\Http\Controllers\WebsiteController::class, 'appledevices'])->name('appledevices');
Route::get('/en/user/manual/readingmode', [App\Http\Controllers\WebsiteController::class, 'readingmode'])->name('readingmode');
Route::get('/en/user/manual/wakeuphelp', [App\Http\Controllers\WebsiteController::class, 'wakeuphelp'])->name('wakeuphelp');
Route::get('/en/user/manual/sleepaid', [App\Http\Controllers\WebsiteController::class, 'sleepaid'])->name('sleepaid');
Route::get('/en/user/manual/lightcontrol', [App\Http\Controllers\WebsiteController::class, 'lightcontrol'])->name('lightcontrol');
Route::get('/en/user/manual/musicmeditations', [App\Http\Controllers\WebsiteController::class, 'musicmeditations'])->name('musicmeditations');
Route::get('/en/user/manual/lightcolortherapy', [App\Http\Controllers\WebsiteController::class, 'lightcolortherapy'])->name('lightcolortherapy');
Route::get('/en/user/manual/personalsleephealthcoach', [App\Http\Controllers\WebsiteController::class, 'personalsleephealthcoach'])->name('personalsleephealthcoach');
Route::get('/en/user/manual/offlineuse', [App\Http\Controllers\WebsiteController::class, 'offlineuse'])->name('offlineuse');
Route::get('/en/user/manual/troubleshooting', [App\Http\Controllers\WebsiteController::class, 'troubleshooting'])->name('troubleshooting');
Route::get('/en/user/manual/storagecare', [App\Http\Controllers\WebsiteController::class, 'storagecare'])->name('storagecare');
Route::get('/en/user/manual/recycling', [App\Http\Controllers\WebsiteController::class, 'recycling'])->name('recycling');
Route::get('/en/user/manual/warrantyandsupport', [App\Http\Controllers\WebsiteController::class, 'warrantyandsupport'])->name('warrantyandsupport');
Route::get('/en/user/manual/technicaldata', [App\Http\Controllers\WebsiteController::class, 'technicaldata'])->name('technicaldata');

Route::get('/en/user/manual/updatefirmware', [App\Http\Controllers\WebsiteController::class, 'updatefirmware'])->name('updatefirmware');


Route::get('/de/user/manual', [App\Http\Controllers\WebsiteController::class, 'userManualDe'])->name('user.manualDe');
Route::get('/de/user/manual/introduction', [App\Http\Controllers\WebsiteController::class, 'deIntroduction'])->name('de.introduction');
Route::get('/de/user/manual/generaldescription', [App\Http\Controllers\WebsiteController::class, 'deGeneraldescription'])->name('de.generaldescription');
Route::get('/de/user/manual/intendeduse', [App\Http\Controllers\WebsiteController::class, 'deIntendeduse'])->name('de.intendeduse');
Route::get('/de/user/manual/colorlighttherapy', [App\Http\Controllers\WebsiteController::class, 'deColorlighttherapy'])->name('de.colorlighttherapy');
Route::get('/de/user/manual/safetyimportant', [App\Http\Controllers\WebsiteController::class, 'deSafetyimportant'])->name('de.safetyimportant');
Route::get('/de/user/manual/electromagnetic', [App\Http\Controllers\WebsiteController::class, 'deElectromagnetic'])->name('de.electromagnetic');
Route::get('/de/user/manual/setupdevice', [App\Http\Controllers\WebsiteController::class, 'deSetupdevice'])->name('de.setupdevice');
Route::get('/de/user/manual/appledevices', [App\Http\Controllers\WebsiteController::class, 'deAppledevices'])->name('de.appledevices');
Route::get('/de/user/manual/readingmode', [App\Http\Controllers\WebsiteController::class, 'deReadingmode'])->name('de.readingmode');
Route::get('/de/user/manual/wakeuphelp', [App\Http\Controllers\WebsiteController::class, 'deWakeuphelp'])->name('de.wakeuphelp');
Route::get('/de/user/manual/sleepaid', [App\Http\Controllers\WebsiteController::class, 'deSleepaid'])->name('de.sleepaid');
Route::get('/de/user/manual/lightcontrol', [App\Http\Controllers\WebsiteController::class, 'deLightcontrol'])->name('de.lightcontrol');
Route::get('/de/user/manual/musicmeditations', [App\Http\Controllers\WebsiteController::class, 'deMusicmeditations'])->name('de.musicmeditations');
Route::get('/de/user/manual/lightcolortherapy', [App\Http\Controllers\WebsiteController::class, 'deLightcolortherapy'])->name('de.lightcolortherapy');
Route::get('/de/user/manual/personalsleephealthcoach', [App\Http\Controllers\WebsiteController::class, 'dePersonalsleephealthcoach'])->name('de.personalsleephealthcoach');
Route::get('/de/user/manual/offlineuse', [App\Http\Controllers\WebsiteController::class, 'deOfflineuse'])->name('de.offlineuse');
Route::get('/de/user/manual/troubleshooting', [App\Http\Controllers\WebsiteController::class, 'deTroubleshooting'])->name('de.troubleshooting');
Route::get('/de/user/manual/storagecare', [App\Http\Controllers\WebsiteController::class, 'deStoragecare'])->name('de.storagecare');
Route::get('/de/user/manual/recycling', [App\Http\Controllers\WebsiteController::class, 'deRecycling'])->name('de.recycling');
Route::get('/de/user/manual/warrantyandsupport', [App\Http\Controllers\WebsiteController::class, 'deWarrantyandsupport'])->name('de.warrantyandsupport');
Route::get('/de/user/manual/technicaldata', [App\Http\Controllers\WebsiteController::class, 'deTechnicaldata'])->name('de.technicaldata');

Route::get('/de/user/manual/updatefirmware', [App\Http\Controllers\WebsiteController::class, 'deUpdatefirmware'])->name('de.updatefirmware');








Route::get('/home', function () {
    return redirect('keyword');
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/languages',  App\Http\Controllers\LanguagesController::class);
    Route::resource('/keyword', App\Http\Controllers\KeywordController::class);
    Route::resource('/content', App\Http\Controllers\ContentController::class);
    Route::post('ckeditor/image_upload', [App\Http\Controllers\WebsiteController::class, 'upload'])->name('upload');
    
    // Route::post('logout', 'Auth\LoginController@logout');
});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
