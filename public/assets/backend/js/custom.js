$(() => {
    $(".new-product-carousel").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        navText: [
            "<i class='fas fa-angle-left'></i>",
            "<i class='fas fa-angle-right'></i>",
        ],
    });

    $(".new-variety-carousel").owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        navText: [
            "<i class='fas fa-angle-left'></i>",
            "<i class='fas fa-angle-right'></i>",
        ],
    });

    /* DATATAble Init */
    if ($(".data-table").length > 0) {
        $(".data-table").DataTable({
            language: {
                paginate: {
                    next: '<i class="bx bx-right-arrow-alt font-size-22">', // or '→'
                    previous: '<i class="bx bx-left-arrow-alt font-size-22">', // or '←'
                },
            },
        });
    }
    if ($(".data-table-no-info").length > 0) {
        $(".data-table-no-info").DataTable({
            searching: false,
            paging: false,
            info: false,
        });
    }

    $(".modal").on("shown.bs.modal", function () {
        $(this).find("[autofocus]").get(0).focus();
    });
});
