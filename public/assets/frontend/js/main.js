$(function(){
    $('.selectpicker').selectpicker();
});

/* ---------------------
	SCROLL PARALLAX EFFECT
---------------------- */
var images = document.querySelectorAll(".thumbnail");
new simpleParallax(images, {
	delay: 1,
	scale: 6,
	overflow: true,
	transition: "cubic-bezier(0,0,0,1)",
});

/* ---------------------
	SCROLL REVEAL EFFECT
---------------------- */
ScrollReveal({
	reset: true,
});
ScrollReveal().reveal(".fade-in", {
	duration: 5000,
	move: 0,
});
ScrollReveal().reveal(".slide-up", {
	duration: 2000,
	origin: "bottom",
	distance: "100px",
	easing: "cubic-bezier(.37,.01,.74,1)",
	opacity: 0.3,
	scale: 0.5,
});
ScrollReveal().reveal(".fade-up", {
	duration: 1000,
	origin: "bottom",
	distance: "10px",
	easing: "cubic-bezier(0.5, 0, 0, 1)",
	/* rotate: {
		x: 20,
		z: -10,
	}, */
});

/* ---------------------
	ON SCROLL ANIMATIONS WITH GSAP
---------------------- */
var audio;
document.addEventListener("DOMContentLoaded", () => {
	let controller = new ScrollMagic.Controller();

	let timeline = new TimelineMax();
	timeline.to(".main-product", 1, {
		rotation: 120,
		position: "fixed",
		zIndex: 99,
		scrub: true,
		autoAlpha: 0,
		ease: Power3.easeInOut,
	});
	let scene = new ScrollMagic.Scene({
		triggerElement: ".hero-wrap",
		duration: "140%",
		triggerHook: 0.15,
		offset: "0",
	})
		.setTween(timeline)
		/* .addIndicators({
			colorTrigger: "black",
			colorStart: "blue",
			colorEnd: "red",
			indent: 10
		}) */
		.addTo(controller);


	/* Pillow Animate */
	let tlPillow = new TimelineMax();
	tlPillow.to("#pillow img", 1, {
		scale: 1,
		ease: Power3.easeInOut,
	});
	let scenePillow = new ScrollMagic.Scene({
		triggerElement: "#pillow",
		duration: "85%",
		triggerHook: 0.75,
		offset: "0",
	})
		.setTween(tlPillow)
		.addTo(controller);

	/* timeline 2 */
	let timeline2 = new TimelineMax();
	timeline2.to(".section-2 .pine-tree", 1, {
		scale: 1,
	});
	let scene2 = new ScrollMagic.Scene({
		triggerElement: ".section-2",
		duration: "70%",
		triggerHook: 0.7,
		offset: "0",
	})
		.setTween(timeline2)
		.addTo(controller);

	/* timeline 3 */
	let timeline3 = new TimelineMax();
	timeline3.to(".self-therapy .lamp", 1, {
		scale: 1,
		ease: Power3.easeInOut,
	});

	let scene3 = new ScrollMagic.Scene({
		triggerElement: ".self-therapy",
		duration: "70%",
		triggerHook: 1,
		offset: "0",
	})
		.setTween(timeline3)
		.addTo(controller);

	/* timeline 4 */
	let timeline4 = new TimelineMax();

	timeline4
		.to(
			".assembly-intro",
			3,
			{
				'transform': 'translateX(100%)',
				//opacity: 0,
				ease: Power3.easeInOut,
			},
			"-=1"
		)
		.to(".assembly-section .top", 1.2, {
			y: "-40vh",
			ease: Power3.easeInOut,
		})
		.to(
			".top-detail",
			1,
			{
				opacity: 1,
			},
			"-=1"
		)
		.to(
			".assembly-section .circuit-cover",
			1.4,
			{
				y: "-32vh",
				opacity: 1,
				ease: Power3.easeInOut,
			},
			"-=1"
		)
		.to(
			".assembly-section .circuit",
			1.6,
			{
				y: "-23vh",
				opacity: 1,
				ease: Power3.easeInOut,
			},
			"-=1"
		)
		.to(
			".base-detail",
			1,
			{
				opacity: 1,
			},
			"-=0.5"
		)
		.to(
			".assembly-list",
			0,
			{
				"z-index": "4",
			}
		)
		
		/* .to(".panel.cusion-wrap", 1, {
			"z-index": "5",
			opacity: 1,
			ease: Linear.easeNone,
		})
		.to(".panel .lamp-cusion", 2, {
			opacity: 1,
			"z-index": "99",
			position: "relative",
			ease: Linear.easeNone,
		}).to(".panel .lamp-cusion-details", 2, {
			transform: 'translateX(0)',
			ease: Linear.easeNone,
		}) */;

	let scene4 = new ScrollMagic.Scene({
		triggerElement: ".assembly-section",
		duration: "150%",
		scrub: true,
		triggerHook: 0,
		offset: "0",
	})
		.setTween(timeline4)
		.setPin(".assembly-section")
		.addTo(controller);

	/* timeline 5 */
	let timeline5 = new TimelineMax();
	timeline5.to(".section-5 .section-5-banner", 1, {
		"clip-path": "circle(70.7% at 50% 50%)",
		ease: Power3.easeInOut,
	});

	let scene5 = new ScrollMagic.Scene({
		triggerElement: ".section-5",
		duration: "100%",
		scrub: true,
		triggerHook: 0,
		offset: "180px",
	})
		.setTween(timeline5)
		.setPin(".section-5")
		.addTo(controller);

	/* timeline 6 */

	let timeline6 = new TimelineMax();
	timeline6
		.to(".day-night-view .sunrise, .day-night-view .sunrise-top", 1, {
			opacity: 1,
			x: '180%',
			y: "-50%",
			ease: Power3.easeInOut,
		}, '-=1')
		.to(".day-night-view .sun-details", 1, {
			y: "-50%",
			opacity: 1,
			ease: Power3.easeInOut,
		}, '-=1')
		.to(".day-night-view .day-glow", 1, {
			opacity: 1,
			transform: "scale(1.5)",
			y: -100,
			ease: Power3.easeInOut,
		}, "-=1")
		.to(".day-night-view .sunrise, .day-night-view .sunrise-top, .day-night-view .sun-details", 1, {
			delay: 2,
			x: '240%',
			y: "0%",
			opacity: 0,
			ease: Power3.easeInOut,
		}, '-=1')
		
		.to(".day-night-view .day-glow", 1, {
			opacity: 0,
			transform: "scale(0)",
			y: 0,
			ease: Power3.easeInOut,
		}, "-=1")
		.to(".day-night-view .day, .day-night-view .day-mountain", 1, {
			'filter': 'brightness(0.15)',
			ease: Power3.easeInOut,
		}, '-=1')
		
		.to(".day-night-view .moon", 1, {
			opacity: 1,
			x: '590%',
			y: "-145%",
			ease: Power3.easeInOut,
		}).to(".day-night-view .moon-details", 1, {
			y: "-50%",
			opacity: 1,
			ease: Power3.easeInOut,
		}, '-=1')
		.to(".day-night-view .night-glow", 1, {
			zIndex: 5,
			opacity:1,
			ease: Power3.easeInOut,
		}, "-=1")
		.to(".day-night-view .moon", 1, {
			opacity: 0,
			x: '790%',
			y: "0%",
			ease: Power3.easeInOut,
		})
		.to(".day-night-view .day-mountain, .day-night-view .day, .day-night-view .moon-details", 1, {
			opacity: 0,
			ease: Power3.easeInOut,
		},'-=1')
		.to(".day-night-view .meditation", 1, {
			opacity: 1,
			ease: Power3.easeInOut,
		},'-=1')
		.to(".day-night-view .meditation-details", 1, {
			y: "-50%",
			opacity: 1,
			ease: Power3.easeInOut,
		}, '-=1');
		

	let scene6 = new ScrollMagic.Scene({
		triggerElement: ".day-night-view",
		duration: "100%",
		scrub: true,
		triggerHook: 0,
		offset: "0",
	});
	scene6.on("enter", function () {
		playAudioSlide1();
	});
	scene6
		.on("leave", function () {
			stopAudio();
		})
		.setTween(timeline6)
		.setPin(".day-night-view")
		.addTo(controller);


	/* timeline 7 */
	let timeline7 = new TimelineMax();
	timeline7.to(".therapy-section .bg-therapy", 1, {
		"clip-path": "circle(70.7% at 50% 50%)",
		ease: Power3.easeInOut,
	});
	let scene7 = new ScrollMagic.Scene({
		triggerElement: ".therapy-section",
		duration: "70%",
		triggerHook: 0.5,
		offset: "0",
	})
		.setTween(timeline7)
		//.setPin(".therapy-section .bg-therapy")
		.addTo(controller);


	/* timeline 8 */
	/* let timeline8 = new TimelineMax();
	timeline8.to(".tree-rotation .one", 1, {
		x: -520,
		y: -470,
		ease: Power3.easeInOut,
	}).to(".tree-rotation .two", 1, {
		x: 820,
		y: -220,
		ease: Power3.easeInOut,
	}, '-=1').to(".tree-rotation .three", 1, {
		x: -420,
		y: 290,
		scale: 1.6,
		ease: Power3.easeInOut,
	}, '-=1');


	let scene8 = new ScrollMagic.Scene({
		triggerElement: ".tree-rotation",
		duration: "150%",
		triggerHook: 1,
		offset: "0",
	})
		.setTween(timeline8)
		.addTo(controller); */

	/* timeline 8 */
	let timeline9 = new TimelineMax();
	timeline9.to(".feature-intro",2, {
			opacity: 1,
			ease: Power3.easeInOut,
		}).to(".features .fpanel.one", 1, {
			opacity: 1,
			zIndex:9,
		ease: Linear.easeNone,
	}).to(".features .fpanel.two", 1, {
		opacity: 1,
		zIndex:12,
		delay: 1.5,
		ease: Linear.easeNone,
	}).to(".features .fpanel.one", 0, {
		opacity: 0,
		y: -100,
		ease: Linear.easeNone,
	}, "-=1").to(".features .fpanel.three", 1, {
		opacity: 1,
		delay: 1.5,
		zIndex:14,
		ease: Linear.easeNone,
	}).to(".features .fpanel.two", 0, {
		opacity: 0,
		y: -100,
		ease: Linear.easeNone,
	}, "-=1").to(".features .fpanel.four", 1, {
		opacity: 1,
		delay: 1.5,
		zIndex:16,
		ease: Linear.easeNone,
	}).to(".features .fpanel.three", 0, {
		opacity: 0,
		y: -100,
		ease: Linear.easeNone,
	}, "-=1").to(".features .fpanel.five", 1, {
		opacity: 1,
		delay: 1.5,
		zIndex:18,
		ease: Linear.easeNone,
	}).to(".features .fpanel.four", 0, {
		opacity: 0,
		y: -100,
		ease: Linear.easeNone,
	}, "-=1").to(".features .fpanel.six", 1, {
		opacity: 1,
		delay: 1.5,
		zIndex:20,
		ease: Linear.easeNone,
	}).to(".features .fpanel.five", 0, {
		opacity: 0,
		y: -100,
		ease: Linear.easeNone,
	}, "-=1");


	let scene9 = new ScrollMagic.Scene({
		triggerElement: ".features",
		duration: "300%",
		triggerHook: 0,
		offset: "0",
	})
		.setTween(timeline9)
		.setPin(".features")
		.addTo(controller);


	/* pillow package */
	//var width = $(window).width();
	let tlPillowPackage = new TimelineMax();
	if (screen.width < 560) {
        tlPillowPackage.to(".pillow-package .layer-2", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-3-detail", 0.3, {
			opacity: 0,
			display: 'none',
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-detail .layer-2-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-package .layer-1", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-2-detail", 0.3, {
			opacity: 0,
			display: 'none',
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-detail .layer-1-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-package .layer-0", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-1-detail", 0.3, {
			opacity: 0,
			display: 'none',
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-detail .layer-0-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1')
    } else {
        tlPillowPackage.to(".pillow-package .layer-2", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-2-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-package .layer-1", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-1-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1').to(".pillow-package .layer-0", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}).to(".pillow-detail .layer-0-detail", 1, {
			opacity: 1,
			y: 0.1,
			ease: Power3.easeInOut,
		}, '-=1')

	}

	let pillowScene = new ScrollMagic.Scene({
		triggerElement: ".pillow-wrap",
		duration: "100%",
		triggerHook: 0,
		offset: "0",
	})
		.setTween(tlPillowPackage)
		.setPin(".pillow-wrap")
		.addTo(controller);
});

/* MUSIC PLAY/PAUSE FUNCTIONS */
function playAudioSlide1() {
	audio = new Audio(music_img);
	audio.muted = true; 
	audio.play();
	audio.muted = false; 
}
/* function playAudioSlide2() {
	audio = new Audio("./img/02_night.mp3");
	audio.play();
}

function playAudioSlide3() {
	audio = new Audio("./img/03_yoga.mp3");
	audio.play();
} */

function stopAudio() {
	if (audio != undefined) {
		audio.pause();
		audio.currentTime = 0;
	}
}

/* $("#dayNightView").on("slid.bs.carousel", function (event) {
	stopAudio();
	if ("day" == $(event.relatedTarget).attr("data-item-name")) {
		playAudioSlide1();
	} else if ("night" == $(event.relatedTarget).attr("data-item-name")) {
		playAudioSlide2();
	} else if ("meditation" == $(event.relatedTarget).attr("data-item-name")) {
		playAudioSlide3();
	}
}); */

/* SWIPER SLIDER INIT */
const featureSliderDetail = new Swiper(".feature-slider-detail", {
	loop: true,
	spaceBetween: 10,
	slidesPerView: 1,
	freeMode: true,
	watchSlidesProgress: true,
});
const featureSlider = new Swiper(".feature-slider", {
	direction: "horizontal",
	loop: true,
	autoplay: false,
	centeredSlides: true,
	slidesPerView: 2,
	spaceBetween: 50,
	keyboard: true,
	pagination: {
		el: ".swiper-pagination",
		clickable: true,
	},
	thumbs: {
		swiper: featureSliderDetail,
	},
	breakpoints: {
		640: {
			slidesPerView: 3,
			spaceBetween: 20,
		},
		768: {
			slidesPerView: 3,
			spaceBetween: 40,
		},
		1024: {
			slidesPerView: 3,
			spaceBetween: 50,
		},
	},
});

/* CUSTOM TAB FOR A ASSEMBLY SECTION */
$('.assembly-elements .description a, .assembly-elements .description button').on('click', function () {
	$('.assembly-elements .description a, .assembly-elements .description button').removeClass('activelink');
	$(this).addClass('activelink');
	var tagid = $(this).data('tag');
	$('.assembly-list').removeClass('active').addClass('hide');
	$('#' + tagid).addClass('active').removeClass('hide');
});
$('.btn-modal-close').on('click', function() {
	$(this).parent('.assembly-list').removeClass('active');
})

/* NESTED MODAL */
$(".btn-back-aufwachenModal").on( "click", function() {
	$('#aufwachenModal').modal('show');  
});

$(".btn-back-einfschlafenModal").on( "click", function() {
	$('#einfschlafenModal').modal('show');  
});