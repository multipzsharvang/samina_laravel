/*----------------
	LOADER
-----------------*/
document.onreadystatechange = function () {
	if (document.readyState !== "complete") {
		document.querySelector("body").style.visibility = "hidden";
		document.querySelector("#loader").style.visibility = "visible";
	} else {
		document.querySelector("#loader").style.display = "none";
		document.querySelector("body").style.visibility = "visible";
	}
};

/* ---------------------
	SCROLL PARALLAX EFFECT
---------------------- */
var images = document.querySelectorAll(".thumbnail");
new simpleParallax(images, {
	delay: 1,
	scale: 1.5,
	overflow: true,
	transition: "cubic-bezier(0,0,0,1)",
});
/* ---------------------
	SCROLL REVEAL EFFECT
---------------------- */
ScrollReveal({
	reset: true
});
ScrollReveal().reveal(".fade-in", {
	duration: 5000,
	move: 0,
});
ScrollReveal().reveal(".slide-up", {
	duration: 2000,
	origin: "bottom",
	distance: "100px",
	easing: "cubic-bezier(.37,.01,.74,1)",
	opacity: 0.3,
	scale: 0.5,
});
ScrollReveal().reveal(".fade-up", {
	duration: 1500,
	origin: "bottom",
	distance: "80px",
	easing: "cubic-bezier(0.5, 0, 0, 1)",
	/* rotate: {
		x: 20,
		z: -10,
	}, */
});

/* ---------------------
	ON SCROLL ANIMATIONS WITH GSAP
---------------------- */
document.addEventListener("DOMContentLoaded", () => {
	let controller = new ScrollMagic.Controller();

	let timeline = new TimelineMax();
	timeline.to(".main-product", 1, {
		rotation: 45,
		position: "fixed",
		zIndex: 99,
		scrub: true,
		autoAlpha: 0,
		ease: Power3.easeInOut,
	});
	let scene = new ScrollMagic.Scene({
			triggerElement: ".hero-wrap",
			duration: "100%",
			triggerHook: 0.15,
			offset: "0",
		})
		.setTween(timeline)
		.addTo(controller);

	/* timeline 2 */
	let timeline2 = new TimelineMax();
	timeline2.to(
		".section-2 .pine-tree",
		1, {
			scale: 1,
		},
		"-=0.5"
	);
	let scene2 = new ScrollMagic.Scene({
			triggerElement: ".section-2",
			duration: "100%",
			triggerHook: 0.5,
			offset: "0",
		})
		.setTween(timeline2)
		.addTo(controller);

	/* timeline 3 */
	/* let timeline3 = new TimelineMax();
	timeline3.to(".self-therapy .lamp", 1, {
		opacity: [0.3, 1],
		scale: [4, 1],
		ease: Power3.easeInOut,
	});

	let scene3 = new ScrollMagic.Scene({
			triggerElement: ".self-therapy",
			duration: "100%",
			triggerHook: 1,
			offset: "0",
		})
		.setTween(timeline3)
		.addTo(controller); */

	//------------------
	//TIMELINE 2
	//------------------

	// Add timeline
	let tl2 = anime.timeline({
		autoplay: false
	});

	// Add animations
	let s2a1 = {
		targets: '.self-therapy .lamp',
		opacity: [0.1, 1],
		scale: [4, 1],
		duration: 1000,
		delay: 0,
		easing: 'easeInOutSine'
	};

	let s2a2 = {
		targets: '.self-therapy .lamp',
		scale: 1,
		duration: 2000,
	};

	// Add children
	tl2.add(s2a1).add(s2a2);



	//------------------
	//SCENE 2
	//------------------

	//Add second scrollmagic scene
	let scene3 = new ScrollMagic.Scene({
			triggerElement: ".self-therapy",
			duration: 4500,
			triggerHook: 0,
		})

		// Add debug indicators
		.addIndicators({
			colorTrigger: "black",
			colorStart: "blue",
			colorEnd: "red",
			indent: 10
		})

		// Trigger animation timeline
		//Use scroll position to play animation
		.on("progress", function (event) {
			tl2.seek(tl2.duration * event.progress);
		})

		.setPin('.self-therapy')
		.addTo(controller);

	/*
	let timeline4 = new TimelineMax();
	timeline4
		.to(".section_4_01", 4, {
			autoAlpha: 0,
		})
		.from(
			".section_4_02",
			4,
			{
				autoAlpha: 0,
			},
			"-=4"
		)
		.from(".section_4_03", 4, {
			autoAlpha: 0,
		})
		.from(".section_4_04", 4, {
			autoAlpha: 0,
		});

	let scene4 = new ScrollMagic.Scene({
		triggerElement: ".forth-section",
		duration: "100%",
		triggerHook: 0,
		offset: "200",
	})
		.setTween(timeline4)
		.setPin(".forth-section")
		.addTo(controller); */
});